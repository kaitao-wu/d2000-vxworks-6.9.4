/* excALib.s - exception handling ARM assembly language routines */

/*
 * Copyright (c) 1996-2001, 2004-2008, 2010-2014 Wind River Systems, Inc.
 *
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
15aug14,c_l  Fix for ARM Cortex-A9 Errata#775420. (VXW6-83250)
02w,25jun13,cfm  OSM should only deal with exception and interrupt stacks.
                 (WIND00421208)
02v,20feb13,m_h  CortexA15 GuestOS support through VSB configuration
02u,16oct12,jdw  Add SPSR set macro (WIND00259770)
02t,13sep12,ggm  added getVBAR and setVBAR functions.
02s,30mar12,rec  WIND00343205 - System calls from kernel don't raise exception
                 WIND00342565 - Thumb2 targets failing to report a branch
                   through zero (switch to ARM mode).
                 WIND00342537 - Thumb2 targets reporting incorrect information
02r,07feb12,rec  WIND00328880 - Invalid SWI status register contents
02q,18jan12,rec  WIND00329076 - fix stack backtrace.  General cleanup
02p,01jun11,m_h  clear F_BIT early in L$excEnterCommon (WIND00266133)
02o,24feb11,j_b  clear exclusive access locks after Data Abort, per ARM ARM
02n,01feb11,jdw  Change writes to SPSR to mask fields (WIND00253307)
02m,23nov10,j_b  add unified FIQ/IRQ handling support
02l,29sep10,rab  Guest OS support
02k,19jan10,fao  Add be8 support.
02j,20may10,m_h  Thumb-2 support
02i,27aug08,jpb  Renamed VSB config file.
02h,18jun08,jpb  Added include path for kernel configurations options set in
                 vsb.
02g,22apr08,m_h  NOP after STM^ to seperate banked register
02f,24mar08,m_h  Support for ARM 7tdmi (MMUless)
02e,21may08,jpb  Modifications for source build.  Renamed INCLUDE_MMU_BASIC to
                 _WRS_CONFIG_MMU_BASIC, _WRS_VX_SMP to _WRS_CONFIG_SMP.
02d,16jul07,j_b  omit addr storage in vxKernelVars for UP in
                 armInitExceptionModes
02c,07jun07,j_b  restore r4 in excEnterCommon2, fix ascii and lack of newline
02b,09may07,m_h  SMP SaveArea Init Issues
02a,18apr07,m_h  SMP Globals
01z,09oct06,j_b  Account for 'base restored data abort model' (Defect 64790)
01y,18jul06,jb   Fix pre-kernel bringup
01x,30mar06,ajc  SPR 119277: update sysExcMsg pointer
01w,22feb06,m_h  revert WDB pre-kernel init bug changes
01v,27jan06,jb   Fix WDB pre-kernel init bug
01u,15aug05,rhe  Replaced ksprintf with sprintf
01t,08aug05,h_k  added #ifdef INCLUDE_MMU_BASIC.
01s,08jul05,h_k  added layering.
01r,19apr05,scm  fix SPR107442...
01q,22mar05,scm  handle interrupt stack protection.
01p,24feb05,jb   Fix for SPR 106519
01o,17feb05,scm  move to exception stack, add initial OSM support...
01n,07sep04,scm  move to exception stack adjust excCnt...
01m,17oct01,t_m  "test FUNC_LABEL"
01l,11oct01,jb   Enabling removal of pre-pended underscores for new compilers
                 (Diab/Gnu elf)
01l,25sep01,rec  archv5 support
01k,23jul01,scm  change XScale name to conform to coding standards...
01j,04may01,scm  add STRONGARM support...
01i,11dec00,scm  replace references to ARMSA2 with XScale
01h,13nov00,scm  modify for SA2
01g,06jul99,cdp  insert NOP after LDM to user bank (SPR #28011).
01f,20jan99,cdp  removed support for old ARM libraries.
01e,06jul98,cdp  added support for generic ARCH3/4/4_T.
01d,27oct97,kkk  took out "***EOF***" line from end of file.
01c,23sep97,cdp  removed kludges for old Thumb tool-chains.
01b,26mar97,cdp  added (Thumb) ARM7TDMI_T support, new BTZ code, IMB handling.
01a,09may96,cdp  written.
*/

/*
DESCRIPTION
This module contains the assembly-language exception-handling stubs
which are connected directly to the ARM exception vectors. Each stub
sets up an appropriate environment and then calls a routine in
excArchLib(1).


SEE ALSO: excArchLib(1)
*/

#define _ASMLANGUAGE
#include <vxWorks.h>
#include <vsbConfig.h>

#include <private/taskLibP.h>
#include <arch/arm/asmArm.h>
#include <arch/arm/archArm.h>     /* For _WRS_OSM_INIT */
#include <arch/arm/excArmLib.h>
#include <private/vxSmpP.h>

#ifndef _WRS_CONFIG_MMU_BASIC
#undef  _WRS_OSM_INIT
#endif  /* _WRS_CONFIG_MMU_BASIC */

#if defined (_WRS_OSM_INIT)
#include <sysLib.h>               /* for BOOT_NORMAL */
#include <vmLib.h>
#include <errno.h>
#include <regs.h>
#endif  /* _WRS_OSM_INIT */
#define _WRS_CONFIG_MULTI_CLUSTERS
        /* globals */

        FUNC_EXPORT(armInitExceptionModes) /* initialise ARM modes */
        FUNC_EXPORT(excEnterUndef)         /* undefined instruction handler */
        FUNC_EXPORT(excEnterSwi)           /* software interrupt handler */
        FUNC_EXPORT(excEnterPrefetchAbort) /* prefetch abort handler */
        FUNC_EXPORT(excEnterDataAbort)     /* data abort handler */

#if (_VX_CPU == _VX_ARMARCH7)
    FUNC_EXPORT(excVBARGet)            /* get exception base address */
    FUNC_EXPORT(excVBARSet)            /* set exception base address */
#endif

        /* externs */

#ifdef _WRS_CONFIG_SMP
    DATA_IMPORT(vxKernelVars)
#else /*_WRS_CONFIG_SMP*/
        DATA_IMPORT(taskIdCurrent)
#endif /*_WRS_CONFIG_SMP*/

        FUNC_IMPORT(excExcContinue)

#if defined (_WRS_OSM_INIT)
#ifndef _WRS_CONFIG_SMP
        DATA_IMPORT(intCnt)
#endif /*!_WRS_CONFIG_SMP*/

#if defined(_ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK)
#ifndef _WRS_CONFIG_SMP
        DATA_IMPORT(vxIntStackEnd)           /* interrupt stack end */
        DATA_IMPORT(vxIntStackBase)          /* interrupt stack base */
#endif /*!_WRS_CONFIG_SMP*/
        DATA_IMPORT(vxIntStackOverflowSize)  /* interrupt overflow region */
        DATA_IMPORT(vxIntStackUnderflowSize) /* interrupt underflow region */
#endif  /* _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK */

        DATA_IMPORT(osmGuardPageSize)   /* saved guard page size */

        FUNC_IMPORT(excVmStateSet)
        FUNC_IMPORT(sprintf)            /* prep emergency message */

        FUNC_IMPORT(sysToMonitor)       /* emergency exit: REBOOT */

        DATA_IMPORT(sysExcMsg)          /* emergency msg */
#endif  /* _WRS_OSM_INIT */

#if (ARM_THUMB2)
        FUNC_IMPORT(vxTaskEntryFatalInject)
#endif /* (ARM_THUMB2) */

#if ((_VX_CPU == _VX_ARMARCH6) && (!defined _WRS_CONFIG_SMP))

/* address for dummy STREX to clear exclusive access lock, for uni-proc ARMv6 */ 
    DATA_IMPORT(_dummy_strex)

#endif /* (_VX_CPU == _VX_ARMARCH6) && (!defined _WRS_CONFIG_SMP) */

    .data
        .balign 4

#if defined (_WRS_OSM_INIT)
/********************************************************************************
* osmFatalMsg - excOsm FATAL ERROR Message used prior to call to sysToMonitor.
*
*/

VAR_LABEL(osmFatalMsg)
        .ascii   "excOsm:FATAL: Bad stack pointer,"
        .asciz   " errno = 0x%08x\n"

        .balign 4

#endif  /* _WRS_OSM_INIT */

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)

#define EXTRA_STACK          6       /* extra stack space for _ARM_CPU_INDEX_GET */

#else  /* !_WRS_CONFIG_MULTI_CLUSTERS || !_WRS_CONFIG_SMP */

#define EXTRA_STACK          0

#endif /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP  */

#ifdef _WRS_CONFIG_SMP
/*
 * Save areas for ARM exception modes.  This is repeated for all four CPUs.
 * Note: the r13 of the relevant exception mode points to the start
 * of the save area - this is not a stack.
 */

#define _ARM_PER_CPU_CONCAT_CPU(label, cpu) L$_##label##_##cpu
#define IMM1 #1
#define IMM2 #2
#define IMM3 #3
#define IMM4 #4
#define IMM5 #5
#define IMM6 #6
#define IMM7 #7

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)

#define _ARM_CPU_INDEX_GET_SP(r)           \
        MRC     p15, 0, r4, c0, c0, 5            ; \
        bic r4, r4, IMM 0xff000000       ; \
        mov r5, IMM 0            ; \
        ldr r6, =cpuIndexMap         ; \
99999:                       ; \
        ldr r7, [r6], IMM 4          ; \
        mov r, r5                ; \
        cmp r7, r4               ; \
        beq 55555f               ; \
        add r5, r5, IMM 1            ; \
        cmp r5, IMM VX_MAX_SMP_CPUS      ; \
        blt 99999b               ; \
        mvn r, IMM 0             ; \
55555:                       ; \
/* place the address of label into register r */

#define _ARM_PER_CPU_ADRS_GET_SP(r, scratch, label)               \
    _ARM_CPU_INDEX_GET_SP(r)                                ; \
        LDR  scratch, L$_vxKernelVars                           ; \
        ADD  scratch, scratch, r, LSL ARM_WIND_VARS_ALIGN_SHIFT ; \
        ADD  r, scratch, ARM_HASH _ARM_WIND_VARS_OFFSET(label)  ; \

#endif /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP */
/* initialize the per-CPU save area with a pointer to the local address */
#define _ARM_PER_CPU_SAVE_AREA_ADR_GET(r, scratch, label)    \
    _ARM_CPU_INDEX_GET(scratch)                 ;\
    LDR   r, _ARM_PER_CPU_CONCAT_CPU(label,0) /* default area zero*/ ; \
    CMP   scratch, IMM1                                 ; \
    LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,1)           ; \
    CMP   scratch, IMM2                                 ; \
    LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,2)           ; \
    CMP   scratch, IMM3                                 ; \
        LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,3)           ; \
        CMP   scratch, IMM4                                 ; \
        LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,4)           ; \
        CMP   scratch, IMM5                                 ; \
        LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,5)           ; \
        CMP   scratch, IMM6                                 ; \
        LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,6)           ; \
    CMP   scratch, IMM7                                 ; \
        LDREQ r, _ARM_PER_CPU_CONCAT_CPU(label,7) 
/* CPU 0 save areas */

VAR_LABEL(undefSaveArea)
VAR_LABEL(undefSaveArea_0) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

VAR_LABEL(abortSaveArea)
#if defined (_WRS_OSM_INIT)
  .balign 8
  VAR_LABEL(abortSaveArea_0) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_0) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea)
VAR_LABEL(swiSaveArea_0)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack)
VAR_LABEL(irqStack_0)

/*
 * ARM FIQ stack
 */

        .fill   (8 + EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack)
VAR_LABEL(fiqStack_0)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack)
VAR_LABEL(irqStack_0)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

/* CPU 1 save areas */

VAR_LABEL(undefSaveArea_1) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
 .balign 8
 VAR_LABEL(abortSaveArea_1) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_1) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_1)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_1)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_1)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_1)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

/* CPU 2 save areas */

VAR_LABEL(undefSaveArea_2) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea_2) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_2) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_2)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_2)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_2)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_2)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

/* CPU 3 save areas */

VAR_LABEL(undefSaveArea_3) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea_3) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_3) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_3)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_3)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_3)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_3)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

/* CPU 4 save areas */

VAR_LABEL(undefSaveArea_4) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea_4) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_4) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_4)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_4)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_4)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_4)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

/* CPU 5 save areas */

VAR_LABEL(undefSaveArea_5) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea_5) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_5) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_5)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_5)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_5)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_5)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

/* CPU 6 save areas */

VAR_LABEL(undefSaveArea_6) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea_6) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_6) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_6)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_6)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_6)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_6)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */


/* CPU 7 save areas */

VAR_LABEL(undefSaveArea_7) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea_7) .fill 96+ EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea_7) .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea_7)   .fill 7+ EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack_7)

/*
 * ARM FIQ stack
 */

        .fill   (8+ EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack_7)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+ EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack_7)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */
#else /*_WRS_CONFIG_SMP*/

/*
 * Save areas for ARM exception modes.
 * Note: the r13 of the relevant exception mode points to the start
 * of the save area - this is not a stack.
 */

VAR_LABEL(undefSaveArea) .fill 7+EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#if defined (_WRS_OSM_INIT)
VAR_LABEL(abortSaveArea) .fill 96+EXTRA_STACK,4 /* stack expanded for guard page support */
#else
VAR_LABEL(abortSaveArea) .fill 7+EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */
#endif  /* _WRS_OSM_INIT */
VAR_LABEL(swiSaveArea)   .fill 7+EXTRA_STACK,4  /* 7 registers: SPSR,r0-r4,lr */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ
 
/*
 * ARM IRQ stack
 */

        .fill   (8+EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(irqStack)

/*
 * ARM FIQ stack
 */

        .fill   (8+EXTRA_STACK)*2,4       /* 8 registers: CPSR,SPSR,r0-r4,lr */
VAR_LABEL(fiqStack)

#else

/*
 * ARM IRQ stack
 * FIQ is not handled by VxWorks so no FIQ-mode stack is allocated.
 */

        .fill   (7+EXTRA_STACK)*2,4       /* 7 registers: SPSR,r0-r4,lr */
VAR_LABEL(irqStack)

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

#endif /*_WRS_CONFIG_SMP*/


        .text
        .balign 4

#if (ARM_THUMB2)
    .thumb
#else /*(ARM_THUMB2)*/
    .code   32
#endif /*(ARM_THUMB2)*/

/* PC-relative-addressable symbols - LDR Rn,=sym is broken */

#ifdef _WRS_CONFIG_SMP

L$_vxKernelVars:      .long   VAR(vxKernelVars)

L$_undefSaveArea_0:   .long   VAR(undefSaveArea_0)
L$_abortSaveArea_0:   .long   VAR(abortSaveArea_0)
L$_swiSaveArea_0:     .long   VAR(swiSaveArea_0)
L$_irqStack_0:        .long   VAR(irqStack_0)

L$_undefSaveArea_1:   .long   VAR(undefSaveArea_1)
L$_abortSaveArea_1:   .long   VAR(abortSaveArea_1)
L$_swiSaveArea_1:     .long   VAR(swiSaveArea_1)
L$_irqStack_1:        .long   VAR(irqStack_1)

L$_undefSaveArea_2:   .long   VAR(undefSaveArea_2)
L$_abortSaveArea_2:   .long   VAR(abortSaveArea_2)
L$_swiSaveArea_2:     .long   VAR(swiSaveArea_2)
L$_irqStack_2:        .long   VAR(irqStack_2)

L$_undefSaveArea_3:   .long   VAR(undefSaveArea_3)
L$_abortSaveArea_3:   .long   VAR(abortSaveArea_3)
L$_swiSaveArea_3:     .long   VAR(swiSaveArea_3)
L$_irqStack_3:        .long   VAR(irqStack_3)

L$_undefSaveArea_4:   .long   VAR(undefSaveArea_4)
L$_abortSaveArea_4:   .long   VAR(abortSaveArea_4)
L$_swiSaveArea_4:     .long   VAR(swiSaveArea_4)
L$_irqStack_4:        .long   VAR(irqStack_4)

L$_undefSaveArea_5:   .long   VAR(undefSaveArea_5)
L$_abortSaveArea_5:   .long   VAR(abortSaveArea_5)
L$_swiSaveArea_5:     .long   VAR(swiSaveArea_5)
L$_irqStack_5:        .long   VAR(irqStack_5)

L$_undefSaveArea_6:   .long   VAR(undefSaveArea_6)
L$_abortSaveArea_6:   .long   VAR(abortSaveArea_6)
L$_swiSaveArea_6:     .long   VAR(swiSaveArea_6)
L$_irqStack_6:        .long   VAR(irqStack_6)

L$_undefSaveArea_7:   .long   VAR(undefSaveArea_7)
L$_abortSaveArea_7:   .long   VAR(abortSaveArea_7)
L$_swiSaveArea_7:     .long   VAR(swiSaveArea_7)
L$_irqStack_7:        .long   VAR(irqStack_7)
#  ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

L$_fiqStack_0:        .long   VAR(fiqStack_0)
L$_fiqStack_1:        .long   VAR(fiqStack_1)
L$_fiqStack_2:        .long   VAR(fiqStack_2)
L$_fiqStack_3:        .long   VAR(fiqStack_3)
L$_fiqStack_4:        .long   VAR(fiqStack_4)
L$_fiqStack_5:        .long   VAR(fiqStack_5)
L$_fiqStack_6:        .long   VAR(fiqStack_6)
L$_fiqStack_7:        .long   VAR(fiqStack_7)

#  endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

#else /*_WRS_CONFIG_SMP*/

L$_taskIdCurrent:   .long   VAR(taskIdCurrent)
L$_undefSaveArea:   .long   VAR(undefSaveArea)
L$_abortSaveArea:   .long   VAR(abortSaveArea)
L$_swiSaveArea:     .long   VAR(swiSaveArea)
L$_irqStack:        .long   VAR(irqStack)

#  ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

L$_fiqStack:        .long   VAR(fiqStack)

#  endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

#endif /*_WRS_CONFIG_SMP*/

#if defined (_WRS_OSM_INIT)
#ifndef _WRS_CONFIG_SMP
L$_intCnt:                  .long   VAR(intCnt)
L$_errno:                   .long   VAR(errno)
#endif /*_WRS_CONFIG_SMP*/

#if defined(_ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK)
#ifndef _WRS_CONFIG_SMP
L$_vxIntStackEnd:           .long   VAR(vxIntStackEnd)
L$_vxIntStackBase:          .long   VAR(vxIntStackBase)
#endif /*_WRS_CONFIG_SMP*/
L$_vxIntStackOverflowSize:  .long   VAR(vxIntStackOverflowSize)
L$_vxIntStackUnderflowSize: .long   VAR(vxIntStackUnderflowSize)

#endif /* _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK */

L$_osmGuardPageSize:        .long   VAR(osmGuardPageSize)
L$_osmFatalMsg:             .long   VAR(osmFatalMsg)
L$_sysExcMsg:               .long   VAR(sysExcMsg)

#endif  /* _WRS_OSM_INIT */

#if ((_VX_CPU == _VX_ARMARCH6) && (!defined _WRS_CONFIG_SMP))
L$__dummy_strex:            .long  VAR(_dummy_strex)
#endif /* (_VX_CPU == _VX_ARMARCH6) && (!defined _WRS_CONFIG_SMP) */

        .balign 4

/*******************************************************************************
*
* armInitExceptionModes - initialise ARM exception modes
*
* This routine initialises the registers for the ARM exception modes.
*
*
* void armInitExceptionModes (void)
*
* INTERNAL
* The SP used by exception modes does not point to a (full-descending)
* stack but to an area just big enough to hold critical registers before
* the processor is switched to SVC mode for exception processing. The SP
* of the exception mode must be left pointing to the base of this area and
* the values in that area must be copied out so that the next exception can
* use it.
*
* Entered in SVC32 mode.
*/

FUNC_BEGIN(armInitExceptionModes)

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
    stmfd   sp!, {r4 - r7}
#endif /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP */

#ifdef _WRS_CONFIG_SMP

    _ARM_PER_CPU_SAVE_AREA_ADR_GET(r0, r1, undefSaveArea)
    _ARM_PER_CPU_ADRS_GET(r1, r2, undefSaveArea)
    str r0, [r1]    /* store local address into vxKernelVars */

    _ARM_PER_CPU_SAVE_AREA_ADR_GET(r0, r1, abortSaveArea)
    _ARM_PER_CPU_ADRS_GET(r1, r2, abortSaveArea)
    str r0, [r1]    /* store local address into vxKernelVars */

    _ARM_PER_CPU_SAVE_AREA_ADR_GET(r0, r1, swiSaveArea)
    _ARM_PER_CPU_ADRS_GET(r1, r2, swiSaveArea)
    str r0, [r1]    /* store local address into vxKernelVars */

    _ARM_PER_CPU_SAVE_AREA_ADR_GET(r0, r1, irqStack)
    _ARM_PER_CPU_ADRS_GET(r1, r2, irqStack)
    str r0, [r1]    /* store local address into vxKernelVars */

#  ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

    _ARM_PER_CPU_SAVE_AREA_ADR_GET(r0, r1, fiqStack)
    _ARM_PER_CPU_ADRS_GET(r1, r2, fiqStack)
    str r0, [r1]    /* store local address into vxKernelVars */

#  endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

#endif /*_WRS_CONFIG_SMP*/

        MRS     r0,cpsr
        BIC     r1,r0,#MASK_MODE

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r1,r1,#(I_BIT | F_BIT)

#else

        ORR     r1,r1,#I_BIT

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

        /*
         * switch to each mode in turn with interrupts disabled and set SP
         * r0 = original CPSR
         * r1 = CPSR with IRQ/FIQ disabled and mode bits clear
         */

        ORR     r2,r1,#MODE_UNDEF32     /* do UNDEF mode */
        MSR     cpsr,r2

#ifdef ARMBE8
        SETEND BE
        MRS     r3, spsr
        ORR     r3, r3, #E_BIT          /* set BE mode bit*/
        MSR     spsr_x, r3
#endif

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
    _ARM_PER_CPU_ADRS_GET_SP(sp, r2, undefSaveArea)
#else /* !(_WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP) */
    _ARM_PER_CPU_ADRS_GET_SPLR(sp, r2, undefSaveArea)
#endif /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP */


#ifdef _WRS_CONFIG_SMP

        /*
         * The saveArea entry in vxKernelVars is a pointer to the address
         * so now we need to grab the actual address
         */

        LDR     sp, [sp]

#endif /*_WRS_CONFIG_SMP*/

        ORR     r2,r1,#MODE_ABORT32     /* do ABORT mode */
        MSR     cpsr,r2

#ifdef ARMBE8
        SETEND BE
        MRS     r3, spsr
        ORR     r3, r3, #E_BIT          /* set BE mode bit*/
        MSR     spsr_x, r3
#endif

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
    _ARM_PER_CPU_ADRS_GET_SP(sp, r2, abortSaveArea)
#else
    _ARM_PER_CPU_ADRS_GET_SPLR(sp, r2, abortSaveArea)
#endif

#ifdef _WRS_CONFIG_SMP

        /*
         * The saveArea entry in vxKernelVars is a pointer to the address
         * so now we need to grab the actual address
         */

        LDR     sp, [sp]

#endif /*_WRS_CONFIG_SMP*/

        ORR     r2,r1,#MODE_IRQ32       /* do IRQ mode */
        MSR     cpsr,r2

#ifdef ARMBE8
        SETEND BE
        MRS     r3, spsr
        ORR     r3, r3, #E_BIT          /* set BE mode bit*/
        MSR     spsr_x, r3
#endif

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
    _ARM_PER_CPU_ADRS_GET_SP(sp, r2, irqStack)
#else /* !(_WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP) */
    _ARM_PER_CPU_ADRS_GET_SPLR(sp, r2, irqStack)
#endif /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP */

#ifdef _WRS_CONFIG_SMP

        /*
         * The saveArea entry in vxKernelVars is a pointer to the address
         * so now we need to grab the actual address
         */

        LDR     sp, [sp]

#endif /*_WRS_CONFIG_SMP*/

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r2,r1,#MODE_FIQ32       /* do FIQ mode */
        MSR     cpsr,r2

#  ifdef ARMBE8
        SETEND BE
        MRS     r3, spsr
        ORR     r3, r3, #E_BIT          /* set BE mode bit*/
        MSR     spsr_x, r3
#  endif

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
    _ARM_PER_CPU_ADRS_GET_SP(sp, r2, fiqStack)
#else /* !(_WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP) */
    _ARM_PER_CPU_ADRS_GET_SPLR(sp, r2, fiqStack)
#endif /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP */

#  ifdef _WRS_CONFIG_SMP

        /*
         * The saveArea entry in vxKernelVars is a pointer to the address
         * so now we need to grab the actual address
         */

        LDR     sp, [sp]

#  endif /*_WRS_CONFIG_SMP*/


#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

        /* back to SVC mode */

        MSR     cpsr,r0

#ifdef ARMBE8
        MRS     r3, spsr
        ORR     r3, r3, #E_BIT          /* set BE mode bit*/
        MSR     spsr_x, r3
#endif

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
    ldmfd   sp!, {r4 - r7}
#endif  /* _WRS_CONFIG_MULTI_CLUSTERS && _WRS_CONFIG_SMP */
        /*
         **** INTERRUPTS RESTORED
         *
         * zero usr_sp - it should never be used
         */

        MOV     r1,#0
        STMFD   sp!,{r1}
#if (!ARM_THUMB2) /* User mode not supported for Thumb-2 */
        LDMFD   sp,{sp}^        /* Writeback prohibited */
#endif /* (!ARM_THUMB2) */
        ARM_NOP_AFTER_USER_LDM_STM
        ADD     sp,sp,#4        /* correct SP */
        MOV     pc,lr

        FUNC_END(armInitExceptionModes)

/**************************************************************************
*
* excEnterSwi - enter a Software Interrupt exception
*
* This routine is installed on the Software Interrupt vector by excVecInit.
*
* This routine can NEVER be called from C.
*
* SEE ALSO: excVecInit(2)
*
* void excEnterSwi ()
*
* INTERNAL
* MODE = SVC32
* IRQs disabled
*
* This entry stub could put registers directly onto the SVC stack but
* it's done this way (using an intermediate save area) so that it can
* share code with other exceptions.
*/

FUNC_BEGIN(excEnterSwi)

        STMFD   sp!,{r2,r4}        /* save r2 & r4 - there must be SVC stack */

#if (ARM_THUMB2)
        SUB     lr,lr,#2        /* Thumb-2 SWI is 16 bits. */
#else /* ARM instruction set */
        SUB     lr,lr,#4        /* make lr -> faulting instruction */
#endif /* ARM_THUMB */

        /*
         * save some registers in the same order as other exception handlers
         * (see excEnterCommon)
         */

        _ARM_PER_CPU_ADRS_GET (r2, r4, swiSaveArea)

#ifdef _WRS_CONFIG_SMP

        /*
         * The saveArea entry in vxKernelVars is a pointer to the address
         * so now we need to grab the actual address
         */

        LDR     r2, [r2]

#endif /*_WRS_CONFIG_SMP*/

#if (!ARM_THUMB2)
        STMIB   r2,{r0-r4,lr}       /* incorrect r2 & r4 stored */
#else /*(!ARM_THUMB2)*/
    ADD     r2, r2, #4        /* STMIB is illegal in Thumb-2 */
        STMIA   r2,{r0-r4}        /* assembler bug: lr illegal */
        STR     lr, [r2,#4*5]
    SUB     r2, r2, #4        /* adjust r2 back to original */
        /* TODO optimization: the next instruction increments the sp */
#endif /*(!ARM_THUMB2)*/

        /*
         * r2 -> register save area containing <dummy>,r0,r1,<dummy>,r3,<dummy>,lr
         * original r2 on stack
         *
         * now save real r2 value
         */

        LDMFD   sp!,{r1}            /* get original r2 */
        STR     r1,[r2,#4*3]        /* put in save area */

        LDMFD   sp!,{r1}            /* get original r4 */
        STR     r1,[r2,#4*5]        /* put in save area */

        MRS     r3,spsr             /* r3 = svc_spsr */
        STR     r3,[r2]             /* save spsr in save area */
        MOV     r0,#EXC_OFF_SWI     /* r0-> exception vector */

        /*
         * now join main thread with
         * r0-> exception vector
         * r1 = [scratch]
         * r2-> save area (containing spsr,r0-r4,lr)
         * r3 = original CPSR of exception mode
         * r4 = [scratch]
         * MODE=SVC32
         */

        B       FUNC(L$excEnterCommon2)

        FUNC_END(excEnterSwi)

/**************************************************************************
*
* excEnterDataAbort - enter a data abort exception
*
* This routine is installed on the Data Abort exception vector by
* excVecInit.
*
* This routine can NEVER be called from C.
*
* SEE ALSO: excVecInit(2)
*
* void excEnterDataAbort ()
*
* INTERNAL
* MODE = ABORT32
* IRQs disabled
* sp -> register save area (see excEnterCommon)
*/

FUNC_BEGIN(excEnterDataAbort)
#if (_VX_CPU == _VX_ARMARCH7) 

        /* 
         * Errata#775420: A data cache maintenance operation which aborts, 
         * followed by an ISB, without any DSB in-between, might lead to 
         * deadlock. A simple workaround for this erratum is to add a DSB 
         * at the beginning of the abort exception handler.
         */

        DSB
#endif /* (_VX_CPU == _VX_ARMARCH7) */

        /* adjust return address so it points to instruction that faulted */

        SUB     lr,lr,#8

        /* save regs in save area */

#if !(ARM_THUMB2)
        STMIB   sp,{r0-r4,lr}
#else /*!(ARM_THUMB2)*/
        ADD     sp, sp, #4        /* STMIB is illegal in Thumb-2 */
        STMIA   sp,{r0-r4}        /* assembler bug: lr illegal */
        STR     lr, [sp,#4*5]
        SUB     sp, sp, #4        /* adjust sp back to original */
        /* TODO optimization: the next instruction increments the sp */
#endif /*!(ARM_THUMB2)*/

#if ARM_HAS_LDREX_STREX

        /* 
         * The state of the exclusive monitors is UNKNOWN after taking a Data Abort
         * exception, so prevent 'dangling' exclusive access locks.
         */

# if ((_VX_CPU != _VX_ARMARCH6) || (defined _WRS_CONFIG_SMP))

        CLREX                /* CLREX is only available for ARMv6K and above */

# else

        LDR r2, L$__dummy_strex
        STREX   r3, r2, [r2]  /* dummy write to clear exclusive access lock */

# endif /* (_VX_CPU != _VX_ARMARCH6) || (defined _WRS_CONFIG_SMP) */

#endif  /* ARM_HAS_LDREX_STREX */
    
#if defined (_WRS_OSM_INIT)
/*
 * offset stack pointer past ESF info area used by common exception
 * handler to area set aside to be used as OSM Stack...
 *
 *  -----------------------------------------------------------------------
 * | SPSR | r0 | r1 | r2 | r3 | r4 | lr | ...OSM stack area...  ...start * |
 *  -----------------------------------------------------------------------
 * ^                                 (<<<--OSM stack grows down... ) ^
 * | sp moves to point after ESF info area to start of OSM stack  ___|
 *
 * now remaining abort save area can be used as OSM stack...
 *
 *   OSM stack    |                 | <--- ESF info area
 *   used for     -------------------      for abort handling
 *   support in   |                 |
 *   calling      -------------------
 *   vmStateSet   |        |        |
 *                ----     /    -----
 *                ----     /    -----
 *             ^  ----     /    -----
 *             |  |        |        |
 *             |  -------------------
 *    Stack grows | OSM Stack Start | <--- TOS for OSM
 *      Down!!!   -------------------
 *                |                 |
 *
 * If underlying "C" code to implement vmStateSet changes
 * may need to increase stack size to match usage...
 */
        ADD     sp, sp, #96*4
/*
 * "OSM" Stack Checking for guard page violations.
 *
 * Support code for ARM specific implementation of OSM event handling.
 *
 * If the Kernel/User stack becomes corrupted and forces a page fault to
 * occur, the OSM stack (in this case the Abort stack) implementation provides
 * an "always there" stack which will allow us to properly handle the generated
 * fault.
 *
 * A memory abort is signalled by the memory system, activating an abort
 * in response to a data access (Load or Store), by marking the data as invalid.
 * A data abort exception will switch into Abort Mode on the Arm Architecture.
 * The Arm implementation of OSM will exploit the Abort Mode and it's register
 * set to handle OSM events.
 */

    _ARM_PER_CPU_VALUE_GET (r2, r3, taskIdCurrent) /* r2 -> TCB */
        TEQ     r2, #0
        BEQ     move_back_to_stack   /* if NULL, in pre-kernel */

        LDR     r3, L$_osmGuardPageSize
        LDR     r3, [r3]       /* r3 -> osmGuardPageSize */
        TEQ     r3, #0         /* if NULL, osm guard regions not initialized */

        BNE     check_stack

#if defined(_ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK)
        /*
         * osm stack checking not initialized,
         * check if interrupt overflow guard page initialized
         * NOTE: interrupt stack protection assumed to be available only w/ MMU
         */

        LDR     r3, L$_vxIntStackOverflowSize
        LDR     r3, [r3]       /* r3 -> vxIntStackOverflowSize */
        TEQ     r3, #0         /* if NULL, interrupt overflow not initialized */

        BNE     check_stack

        /*
         * interrupt overflow guard page not initialized,
         * check if interrupt underflow guard page intitialized
         */

        LDR     r3, L$_vxIntStackUnderflowSize
        LDR     r3, [r3]       /* r3 -> vxIntStackUnderflowSize */
        TEQ     r3, #0         /* if NULL, interrupt undeflow not initialized */

        BNE     check_stack
#endif  /* _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK */

        B     move_back_to_stack

excOsmReBoot:

        /*
         * sysExcMsg += sprintf(sysExcMsg, osmFatalMsg, errno);
         *
         * while (1)
         *     sysToMonitor(BOOT_NORMAL);
         */

        LDR     r0, L$_sysExcMsg
        LDR     r0, [r0]           /* r0 -> sysExcMsg */
        LDR     r1, L$_osmFatalMsg /* r1 -> pointer to osmFatalMsg */

    _ARM_PER_CPU_VALUE_GET (r2, r1, errno) /* r2 -> errno */
        BL      sprintf
        LDR     r1, L$_sysExcMsg
        LDR     r1, [r1]
        ADD     r1, r1, r0
        LDR     r2, L$_sysExcMsg
        STR     r1, [r2]

        MOV     r0, #BOOT_NORMAL
        BL      sysToMonitor

        B       excOsmReBoot

check_stack:

        /*
         * get the fault address from CP15_FAR (MMU only register) --
         * the banked sp value is unreliable due to the likelihood the core
         * implements the 'base restored abort model': leaving the base register
         * unchanged if data abort occurs in an instruction specifying
         * base register writeback (ex., r13!, {r0, r4});
         * This model is known to be used in ARM9TDMI, XSCALE, and ARM1136JFS;
         * NOT in ARM7TDMI, where the 'base updated abort model' is implemented
         * so lr is preserved here in the case of the ARM7TDMI.
         */

#ifndef _WRS_CONFIG_WRHV_COPROCESSOR_CTRL
#ifndef ARMCPUMMULESS
        MRC     CP_MMU, 0, r14, c6, c0, 0
#endif /*ARMCPUMMULESS*/
#endif /* _WRS_CONFIG_WRHV_COPROCESSOR_CTRL */

        /*
         * determine if stack overflow or underflow has occurred
         *
         * r14 <- fault address
         */

        /* get mode fault occurred in */
        MRS     r3, spsr
        AND     r3, r3, #MASK_SUBMODE           /* examine mode bits */
        TEQ     r3, #MODE_SVC32 & MASK_SUBMODE  /* SVC? */

        /* User task, do not check for stack overflow. */

        BNE     move_back_to_stack

        /* get SVC Mode stack pointer */
        /* switch to SVC mode with interrupts (IRQs) disabled */

        MRS     r3, cpsr
        BIC     r1, r3, #MASK_MODE

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r1, r1, #MODE_SVC32 | I_BIT | F_BIT

#else /*_WRS_CONFIG_UNIFIED_FIQ_IRQ*/

        ORR     r1, r1, #MODE_SVC32 | I_BIT

#endif /*_WRS_CONFIG_UNIFIED_FIQ_IRQ*/

        MSR     cpsr, r1

        /*
         * save sp in non-banked reg so can access saved registers after
         * switching back to ABORT mode
         */

        MOV     r1, sp

        /* switch back to ABORT mode (r3) */

        MSR     cpsr, r3

        /* back in ABORT mode */

    /* if intCnt == 0 we are from task */

    _ARM_PER_CPU_VALUE_GET (r0, r3, intCnt)
        TEQ     r0, #0
#if !defined(_ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK)
        /* else just place ESF back on stack */
        BNE     move_back_to_stack    /* from interrupt, skip task stack check */
#else
        /* else check interrupt stack */
        BNE     check_interrupt_stack /* from Interrupt, check int stack */
#endif  /* _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK */

        LDR     r0, [r2, #WIND_TCB_OPTIONS]     /* r0 -> options */
        ANDS    r0, r0, #VX_NO_STACK_PROTECT
        BNE     move_back_to_stack

        /* go and check for event on exception stack */

        LDRNE   r0, [r2, #WIND_TCB_EXC_CNT]     /* get excCnt */
        TEQ     r0, #0
        BEQ     move_back_to_stack  /* Check stack overflow for exception */

        LDR     r0, [r2, #WIND_TCB_PEXC_STK_START]
        LDR     r3, [r2, #WIND_TCB_P_K_STK_END]

        LDR     r2, L$_osmGuardPageSize
        LDR     r2, [r2]            /* r2 -> osmGuardPageSize */

        CMP     r1, r0
        BGE     move_back_to_stack  /* exception stack do not have underflow
                                       guard zone */

        CMP     r14, r0             /* check fault address, then */

        BGT     move_back_to_stack  /* exception stack do not have underflow
                                       guard zone */

        CMP     r1, r3
        BLE     exc_stk_overrun     /* if >= end, then not in base-Guard */

        CMP     r14, r3             /* check fault address, then */
        BGT     move_back_to_stack  /* if >= end, then not in base-Guard */

        SUB     r3, r3, r2          /* offset by guard page */
    CMP     r14, r3
        ADDGT   r3, r3, r2
        BLE     move_back_to_stack  /* if <= base-Guard, fault lies elsewhere */

exc_stk_overrun:

        SUB     r3, r3, r2          /* offset by guard page */
        CMP     r1, r3

        /* actual OSM event, enable region, create ESF frame  */
        /* Note: vmStateSet needs lowest addr in guard region */
        MOV     r0, r3
        BGT     enable_guard_region

        B       move_back_to_stack

#ifdef _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK
check_interrupt_stack:

    _ARM_PER_CPU_VALUE_GET (r0, r2, vxIntStackBase)
    _ARM_PER_CPU_VALUE_GET (r3, r2, vxIntStackEnd)

        LDR     r2, L$_vxIntStackUnderflowSize
        LDR     r2, [r2]            /* r2 -> vxIntStackUnderflowSize */

        CMP     r1, r0
        BGE     int_stk_underrun    /* if < base, then not in base-Guard */

        CMP     r14, r0             /* check fault address, then */
        BLT     check_int_end       /* if < base, then not in base-Guard */

        ADD     r0, r0, r2          /* offset by guard page */
    CMP     r14, r0
        SUBLE   r0, r0, r2
        BGT     check_int_end       /* if > base-Guard, fault lies elsewhere */

int_stk_underrun:

        ADD     r0, r0, r2          /* offset by guard page */
        CMP     r1, r0

        /* actual OSM event, enable region, create ESF frame  */
        /* Note: vmStateSet needs lowest addr in guard region */
        SUB     r0, r0, r2
        BLT     enable_guard_region

        /* else just place ESF back on stack */
        B       move_back_to_stack

check_int_end:

        LDR     r2, L$_vxIntStackOverflowSize
        LDR     r2, [r2]            /* r2 -> vxIntStackOverflowSize */

        CMP     r1, r3
        BLE     int_stk_overrun     /* if >= end, then not in base-Guard */

        CMP     r14, r3             /* check fault address, then */
        BGT     move_back_to_stack  /* if >= end, then not in base-Guard */

        SUB     r3, r3, r2          /* offset by guard page */
    CMP     r14, r3
        ADDGT   r3, r3, r2
        BLE     move_back_to_stack  /* if <= base-Guard, fault lies elsewhere */

int_stk_overrun:

        SUB     r3, r3, r2          /* offset by guard page */
        CMP     r1, r3

        /* actual OSM event, enable region, create ESF frame  */
        /* Note: vmStateSet needs lowest addr in guard region */
        MOV     r0, r3
        BGT     enable_guard_region

        /* else just place ESF back on stack */
        B       move_back_to_stack
#endif  /* _ARCH_SUPPORTS_PROTECT_INTERRUPT_STACK */

enable_guard_region:

        /* save regs on stack prior to calling out... */
        STMDB   sp!, {r0-r12,lr}

        /*
         * Call vmStateSet to make the guard region valid and writable.
         * If an exception stack is involved, it belongs to the current
         * task; for the interrupt stack it should not matter which
         * context is used since all include the kernel.
         *
         * vmStateSet (
         *      NULL,           /@ null context => current task    arg0 @/
         *      pBuf,           /@ lowest addr in guard region     arg1 @/
         *      bytes,          /@ sizeof(guard region)            arg2 @/
         *      VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE,   /@ arg3 @/
         *      VM_STATE_VALID | VM_STATE_WRITABLE              /@ arg4 @/
         *      );
         *
         * Entries placed on OSM stack for re-enabling guard page...
         *  r0 - arg0, 0
         *  r1 - arg1, %esi ---base (underflow) or end (overflow)
         *  r2 - arg2, osmGuardPageSize
         *  r3 - arg3, VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE
         *  sp - arg4, VM_STATE_VALID | VM_STATE_WRITABLE
         */

                       /* sp -> VM_STATE_VALID | VM_STATE_WRITABLE */
        MOV     r3, #VM_STATE_VALID | VM_STATE_WRITABLE
        STMDB   sp!, {r3}
                       /* r3 -> VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE */
        MOV     r3, #VM_STATE_MASK_VALID | VM_STATE_MASK_WRITABLE
                       /* r2 -> Guard Page Size */
        MOV     r1, r0 /* r1 -> base (underflow) or end (overflow) */
        MOV     r0, #0 /* r0 -> null context */

        BL      excVmStateSet
        CMP     r0, #OK
        BNE     excOsmReBoot  /* should never happen, but... */

        LDMIA   sp!, {r3}

        /* restore regs from stack prior to moving on... */
        LDMIA   sp!, {r0-r12,lr}

move_back_to_stack:

        /*
         * move OSM stack pointer to point back to ESF info area...
         *
         *  ---------------------------------------------------------------
         * | SPSR | r0 | r1 | r2 | r3 | r4 | lr | ... start of OSM stack * |
         *  ---------------------------------------------------------------
         * ^                                                       ^
         * | sp moved here to point to ESF info,  was pointng here |
         *
         * now ESF info stack area can be used in common exception handler...
         *
         */

        _ARM_PER_CPU_ADRS_GET_SPLR(r14, r0, abortSaveArea)
        MOV sp, r14

#ifdef _WRS_CONFIG_SMP

        /*
         * The saveArea entry in vxKernelVars is a pointer to the address
         * so now we need to grab the actual address
         */

        LDR     sp, [sp]

#endif /*_WRS_CONFIG_SMP*/

        LDR     r14,[sp,#4*6]        /* restore LR to fault instruction */

#endif  /* _WRS_OSM_INIT */

        /* set r0 -> exception vector and join main thread */

        MOV     r0,#EXC_OFF_DATA
        B       FUNC(L$excEnterCommon)

        FUNC_END(excEnterDataAbort)

/**************************************************************************
*
* excEnterPrefetchAbort - enter a prefetch abort exception
*
* This routine is installed on the Prefetch Abort exception vector
* by excVecInit.
*
* This routine can NEVER be called from C.
*
* SEE ALSO: excVecInit(2)
*
* void excEnterPrefetchAbort ()
*
* INTERNAL
* MODE = ABORT32
* IRQs disabled
* sp -> register save area (see excEnterCommon)
*/

FUNC_BEGIN(excEnterPrefetchAbort)

        /* adjust return address so it points to instruction that faulted */

        SUB     lr,lr,#4

        /* save regs in save area */

#if !(ARM_THUMB2)
        STMIB   sp,{r0-r4,lr}
#else /*(ARM_THUMB2)*/
    ADD     sp, sp, #4        /* STMIB is illegal in Thumb-2 */
        STMIA   sp,{r0-r4}        /* assembler bug: lr illegal */
        STR     lr, [sp,#4*5]
    SUB     sp, sp, #4        /* adjust sp back to original */
#endif /*(ARM_THUMB2)*/

        /* set r0 -> exception vector and join main thread */

        MOV     r0,#EXC_OFF_PREFETCH
        B       FUNC(L$excEnterCommon)

        FUNC_END(excEnterPrefetchAbort)

/**************************************************************************
*
* excEnterUndef - enter an undefined instruction exception
*
* This routine is installed on the Undefined Instruction vector
* by excVecInit.
*
* This routine can NEVER be called from C.
*
* SEE ALSO: excVecInit(2)
*
* void excEnterUndef ()
*
* INTERNAL
* MODE = UNDEF32
* IRQs disabled
* sp -> register save area (see excEnterCommon)
*/

FUNC_BEGIN(excEnterUndef)

#if (ARM_THUMB2)

        /* save regs in save area */

    ADD     sp, sp, #4          /* STMIB is illegal in Thumb-2 */
        STMIA   sp,{r0-r4}          /* assembler bug: lr illegal */

        MRS     r0,spsr             /* get PSR of faulting code */
        TST     r0,#T_BIT           /* Thumb state? */
        SUBNE   lr,lr,#2            /* ..yes */
        SUBEQ   lr,lr,#4            /* ..no, ARM */

        STR     lr, [sp,#4*5]
    SUB     sp, sp, #4          /* adjust sp back to original */

#else /* ARM_THUMB2 */

        /* adjust return address so it points to instruction that faulted */

        SUB     lr,lr,#4

        /* save regs in save area */

        STMIB   sp,{r0-r4,lr}

#endif /* (ARM_THUMB2) */

        /* set r0 -> exception vector and join main thread */

        MOV     r0,#EXC_OFF_UNDEF

        /* FALL THROUGH to excEnterCommon */

        FUNC_END(excEnterUndef)

/*******************************************************************************
*
* excEnterCommon - enter an exception handler
*
* Control passes to this routine from the exception vectors, after the address
* in lr has been adjusted to point to the faulting instruction.
*
* This routine can NEVER be called from C.
*
*
* void excEnterCommon ()
*
* INTERNAL
*
* The exception modes of the ARM have their own stack pointers. However,
* VxWorks exception handlers expect to be called in the context of the faulting
* task so this veneer switches to SVC mode, saving necessary context in an
* exception stack frame.
* The exception stack pointers are only used to point to a few words
* for register saving. THIS IS NOT A STACK and should not be used as one.
* It is laid out as follows
*
*
*  ------------------------------------
* | SPSR | r0 | r1 | r2 | r3 | r4 | lr |
*  ------------------------------------
* ^
* | sp points here
*
*
* Entry:
*    r0 -> exception vector
*    lr -> faulting instruction
*
*/

FUNC_BEGIN(L$excEnterCommon)

        /* save SPSR in save area */

        MRS     r3,spsr
        STR     r3,[sp]

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP) 
        ADD     sp, sp, #((7 + EXTRA_STACK)*4)  /* 7: spsr, r0~r4, lr */
#endif

        _ARM_PER_CPU_VALUE_GET (r4, r1, taskIdCurrent)  /* r4 -> TCB */

#if defined(_WRS_CONFIG_MULTI_CLUSTERS) && defined(_WRS_CONFIG_SMP)
        SUB     sp, sp, #((7 + EXTRA_STACK)*4) /* 7: spsr, r0~r4, lr */
#endif
        /*
         * save sp in non-banked reg so can access saved registers after
         * switching to SVC mode
         */

        MOV     r2,sp

        /* switch to SVC mode with interrupts (IRQs) disabled */

        MRS     r3,cpsr
        BIC     r1,r3,#MASK_MODE

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r1,r1,#MODE_SVC32 | I_BIT | F_BIT

#else /*_WRS_CONFIG_UNIFIED_FIQ_IRQ*/

        ORR     r1,r1,#MODE_SVC32 | I_BIT

#endif /*_WRS_CONFIG_UNIFIED_FIQ_IRQ*/


        MSR     cpsr,r1

FUNC_BEGIN(L$excEnterCommon2)

    /*
     **** INTERRUPTS DISABLED
     *
     * MODE = SVC32
     * r0 -> exception vector
     * r1 = [scratch]
     * r2 -> where exception mode SPSR,r0-r4,lr are saved
     * r3 = CPSR of exception mode
     * r4 = [scratch]
     * lr = svc_lr at time of exception
     *
     * save the following
     *    ttbase
     *    exception vector address
     *    svc_sp
     *    sp of exception mode (save area pointer)
     *    CPSR of exception mode
     * NOTE: if anything else gets added to this (ESF/REG_SET),
     *       the stack addressing later will need adjusting.
     */

        /*
         * check if already on task's exception stack and switch to it if not
         */

    MOV r1, r4
        TEQ     r1, #0
        LDREQ   r4,[r2,#4*5]                    /* restore r4 TODO:optimize */
        BEQ     alreadyOnExcStack               /* In pre-kernel stay on current stack */
        LDR     r1, [r1, #WIND_TCB_EXC_CNT]     /* get exception count */
        TEQ     r1, #0
        MOVNE   r1, sp                          /* r1 = svc_sp at exc time */
        LDRNE   r4,[r2,#4*5]                    /* restore r4 TODO:optimize */
        BNE     alreadyOnExcStack

        /* switch to task's exception stack */
        MOV     r1, r4                          /* r1 -> TCB */
        LDR     r4,[r2,#4*5]                    /* restore r4 TODO:optimize */
        LDR     r1, [r1, #WIND_TCB_P_K_STK_BASE]    /* get stack pointer */
        STMFD   r1!, {sp}                       /* save svc_sp */
        MOV     sp, r1                          /* switch to exc stack */
        LDMFD   sp!, {r1}                       /* get back svc_sp */

alreadyOnExcStack:

        /*
     * r4 must not be dirty here but removing this restriction may
         * allow the code to be more optimized.
         */

        /*
         * MODE: SVC32
         * Save the following
         * r0 -> exception vector
         * r1 = svc_sp at time of exception
         * r2 -> exception mode save area
         * r3 = CPSR of exception mode
         * lr = svc_lr at time of exception
         *
         * NOTE: if anything else gets added to this, the stack
         * addressing later will need adjusting
         */

        STMFD   sp!,{r0-r3}

#ifndef _WRS_CONFIG_WRHV_COPROCESSOR_CTRL
#ifndef ARMCPUMMULESS
        MRC     CP_MMU, 0, r0, c2, c0, 0   /* get CP15_TTBASE to flush out */
#endif /*ARMCPUMMULESS*/
#endif /* _WRS_CONFIG_WRHV_COPROCESSOR_CTRL */

    /* Note: possible optimization. ttbase is meaningless if no MMU */
        STMFD   sp!,{r0}                   /* reg set (ttbase) */

        /*
        * put registers of faulting task on stack in order defined
        * in REG_SET so can pass pointer to C handler
        */

        LDR     r1,[r2,#4*6]        /* get LR of exception mode */
        LDR     r3,[r2]             /* get SPSR of exception mode */
        STMFD   sp!,{r1,r3}
        SUB     sp,sp,#4*11         /* make room for r4..r14 */

        /*
        * check for USR mode exception - SYSTEM is handled as other modes
        * r3 = SPSR of exception mode
        */

        TST     r3,#MASK_SUBMODE

#if (!ARM_THUMB2)
        STMEQIA sp,{r4-r14}^        /* EQ => USR mode */
#else /* (!ARM_THUMB2) */
       /* TODO: User mode not supported in Thumb-2 */
#endif /* (!ARM_THUMB2) */

        BEQ     L$regsSaved

        /*
        * not USR mode so must change to USR mode to get sp,lr
        * SYSTEM mode is also handled this way (but needn't be)
        * r3 = PSR of faulting mode
        */

        MOV     r1,sp               /* r1 -> where to put regs */
        MRS     r0,cpsr             /* save current mode */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r3,r3,#(I_BIT | F_BIT)

#else

        ORR     r3,r3,#I_BIT

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

#if (!ARM_THUMB2)  /* Thumb-2 exceptions are handled in Thumb mode */
        BIC     r3,r3,#T_BIT
#endif /* (!ARM_THUMB2) */
        MSR     cpsr,r3

        /* in faulting mode - interrupts still disabled */

#if (ARM_THUMB2)  /* sp illegal in THUMB-2 STM reg list  */
        STMIA   r1,{r4-r12}         /* save regs */
        STR     sp, [r1,#4*9]
        STR     lr, [r1,#4*10]
#else /* (ARM_THUMB2) */
        STMIA   r1,{r4-r14}         /* save regs */
#endif /* (ARM_THUMB2) */

        /*
        * check if it's SVC mode and, if so, overwrite stored sp.
        * stack pointed to by r1 contains
        *    r4..r14, PC, PSR of faulting mode
        *    address of exception vector
        *    svc_sp at time of exception
        *    sp of exception mode
        *    CPSR of exception mode
        */

        AND     r3,r3,#MASK_SUBMODE             /* examine mode bits */
        TEQ     r3,#MODE_SVC32 & MASK_SUBMODE   /* SVC? */
        LDREQ   r3,[r1,#4*15]                   /* yes, get org svc_sp */
        STREQ   r3,[r1,#4*9]                    /* and overwrite */

        /* switch back to SVC mode with interrupts still disabled (r0) */

        MSR     cpsr,r0

        /* back in SVC mode - interrupts still disabled */

L$regsSaved:

        /* transfer r0-r3 to stack */

#if (!ARM_THUMB2)
        LDMIB   r2,{r0-r3}      /* get other regs */
#else /* (!ARM_THUMB2) */
    ADD     r2, r2, #4      /* LDMIB is illegal in Thumb-2 */
        LDMIA   r2,{r0-r3}      /* get other regs */
    /*
         * r2 was overwritten and is no longer a "stack pointer"
         * Do not adjust back to original.
         */
#endif /* (!ARM_THUMB2) */

        STMFD   sp!,{r0-r3}

        /*
         * exception save area can now be reused
         * stack contains
         *    r0..r14, PC, PSR of faulting mode
         *    address of exception vector
         *    svc_sp at time of exception
         *    sp of exception mode
         *    CPSR of exception mode
         *
         * IRQs: still disabled
         */

        /* increment exception count */

    _ARM_PER_CPU_VALUE_GET (r0, r1, taskIdCurrent)  /* r0 -> TCB */
        TEQ     r0, #0                  /* Check if in pre-kernel */

        LDRNE   r1, [r0, #WIND_TCB_EXC_CNT]
        ADDNE   r1, r1, #1
        STRNE   r1, [r0, #WIND_TCB_EXC_CNT]

        /* restore interrupt state of faulting code */

        LDR     r0,[sp,#4*16]           /* get PSR */
        BIC     r0,r0,#MASK_MODE        /* clear mode bits */
        ORR     r0,r0,#MODE_SVC32       /* select svc32 */
        MSR     cpsr,r0                 /* and write it to CPSR */

        /*
         * INTERRUPTS RESTORED to how they were when exception occurred
         *
         * call generic exception handler
         */

        MOV     r1,sp                   /* r1 -> REG_SET */
        ADD     r0,r1,#4*15             /* r0 -> ESF (PC, PSR, vector) */
        LDR     fp,[r1,#4*11]
        BL      FUNC(excExcContinue)    /* call C routine to continue */

        /* exception handler returned (SVC32)-disable interrupts (IRQs) again */

        MRS     r0,cpsr

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r0,r0,#(I_BIT | F_BIT)

#else

        ORR     r0,r0,#I_BIT

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

        MSR     cpsr,r0

        /*
         * INTERRUPTS DISABLED
         *
         * restore regs from stack, putting some into the exception save area
         */

        LDR     r2,[sp,#4*20]           /* r2 -> exception save area */
        LDMFD   sp!,{r3-r6}             /* get r0-r3 */
#if (!ARM_THUMB2)
        STMIB   r2,{r3-r6}
#else /* (!ARM_THUMB2) */
        ADD     r2, r2, #4              /* STMIB not supported by Thumb-2 */
        STMIA   r2,{r3-r6}
        SUB     r2, r2, #4
#endif /* (!ARM_THUMB2) */

        /* determine mode in which exception occurred so can restore regs */

        LDR     r3,[sp,#4*12]           /* get PSR of faulting mode */
        TST     r3,#MASK_SUBMODE

#if (!ARM_THUMB2)
        LDMEQIA sp,{r4-r14}^            /* EQ => USR mode */
#else /* (!ARM_THUMB2) */
        /* TODO: User mode not supported for Thumb-2 */
#endif /* (!ARM_THUMB2) */

        BEQ     L$regsRestored

        /* exception was not in USR mode so switch to mode to restore regs */

        MRS     r0, cpsr                /* save PSR (SVC32, IRQs disabled) */

        /*
         * r0 = PSR we can use to return to this mode
         * r3 = PSR of faulting mode
         */

        MOV     r1,sp                   /* r1 -> from where to load regs */

#ifdef _WRS_CONFIG_UNIFIED_FIQ_IRQ

        ORR     r3,r3,#(I_BIT | F_BIT)

#else

        ORR     r3,r3,#I_BIT

#endif /* _WRS_CONFIG_UNIFIED_FIQ_IRQ */

#if (!ARM_THUMB2)  /* exceptions are handled in Thumb-mode for Thumb-2 */
        BIC     r3,r3,#T_BIT
#endif /* (!ARM_THUMB2) */

        MSR     cpsr,r3

        /*
         * in faulting mode - interrupts still disabled
         * r1 -> svc stack where r4-r14 are stored
         */

#if (ARM_THUMB2)  /* sp illegal in THUMB-2 LDM reg list  */
        LDMIA   r1,{r4-r12}             /* load regs */
        LDR     sp, [r1,#4*9]
        LDR     lr, [r1,#4*10]
#else /* (ARM_THUMB2) */
        LDMIA   r1,{r4-r14}             /* load regs */
#endif /* (ARM_THUMB2) */

        /*
         * If it's SVC mode, reset sp as we've just overwritten it
         * The correct value is in r1
         */

        AND     r3,r3,#MASK_SUBMODE             /* examine mode bits */
        TEQ     r3,#MODE_SVC32 & MASK_SUBMODE   /* SVC? */
        MOVEQ   sp,r1

        /* switch back to SVC mode with interrupts still disabled (r0) */

        MSR     cpsr,r0

        /* back in SVC mode - interrupts still disabled */

L$regsRestored:

        /* r4..r14 of faulting mode now restored */

        ADD     sp,sp,#4*11         /* strip r4..r14 from stack */

        LDMFD   sp!,{r1,r3}         /* get LR and SPSR of exception mode */
        STR     r1,[r2,#4*5]        /* save LR in exception save area */
        STR     r3,[r2]             /* ..with SPSR */

        /* get the remaining stuff off the stack */

        LDMFD   sp!,{r1}     /* unload ttbase */

        /* decrement excCnt prior to shifting back */

    _ARM_PER_CPU_VALUE_GET (r0, r1, taskIdCurrent)  /* r0 -> TCB */
        TEQ     r0, #0                  /* Check if in pre-kernel */

        LDRNE   r1, [r0, #WIND_TCB_EXC_CNT]      /* get excCnt */
        SUBNE   r1, r1, #1                       /* decrement excCnt */
        STRNE   r1, [r0, #WIND_TCB_EXC_CNT]      /* and store in TCB */

        /*
         * r0 = address of exception vector - discarded
         * r1 = svc_sp at time of exception - discarded
         * r2 = sp of exception mode
         * r3 = CPSR of exception mode
         */

        LDMFD   sp!,{r0-r3}

        MOV     sp, r1            /* restore original svc_sp */

        MSR     cpsr,r3           /* switch back to exception mode */

        /*
         * back in exception mode
         * restore remaining registers and return to task that faulted
         */

        LDR     r1,[r2]

        _ARM_SPSR_SET(r1)

#if (!ARM_THUMB2)
        LDMIB   r2,{r0-r3,pc}^
#else /* (!ARM_THUMB2) */
        LDR     r0, [r2, #4*5]   /* copy PC onto stack */
        STMDB   sp!, {r0,r1}
        ADD     r2, r2, #4       /* LDMIB is illegal for Thumb-2 */
        LDMIA   r2,{r0-r3}       /* restore r0-r3 (not banked) */
        RFEIA   sp!              /* dispatch: restoring PC & CPSR */
#endif /* (!ARM_THUMB2) */

        FUNC_END(L$excEnterCommon)
        FUNC_END(L$excEnterCommon2)

    
#if (_VX_CPU == _VX_ARMARCH7)
    
/**************************************************************************
 *
 * excVBARGet - Read the content of the Vector Base Address register (VBAR)
 *
 * This routine returns the Vector Base Address register (VBAR) in CP15
 *
 * UINT32 excVBARGet(void)
 *
 * RETURNS: value of the VBAR register
 *
 * ERRNO: N/A   
 *
 * \NOMANUAL
 *
 */

FUNC_BEGIN(excVBARGet)
    MRC     p15, 0, r1, c12, c0, 0
    BIC r0, r1, #0x1F       /* r0 = masked address  */
    BX  lr

    FUNC_END(excVBARGet)

/**************************************************************************
 *
 * excVBARSet - Write the value of the Vector Base Address register (VBAR)
 *
 * This routine sets the the Vector Base Address register (VBAR) in CP15 
 * to the specified value
 *
 * void excVBARSet(UINT32 adr)
 *
 * RETURNS: N/A
 *
 * ERRNO: N/A   
 *
 * \NOMANUAL
 *
 */

FUNC_BEGIN(excVBARSet)
    BIC r1, r0, #0x1F       /* r1 = masked address  */
    MCR     p15, 0, r1, c12, c0, 0  /* VBAR = r1 */
    BX  lr

    FUNC_END(excVBARSet)

#endif /* (_VX_CPU == _VX_ARMARCH7) */
