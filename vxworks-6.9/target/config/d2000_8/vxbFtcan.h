/* vxbFtcan.h - CAN driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbftcanh
#define __INCvxbftcanh

#ifdef __cplusplus
extern "C" {
#endif

#define FTCAN_QUEUE_SIZE    1024
#define VXB_FTCAN_MSG_SIZE  64
/* CAN payload length and DLC definitions according to ISO 11898-1 */
#define CAN_MAX_DLC 8
#define CAN_MAX_DLEN        8
#define CAN_MAX_CTL        3
#define CAN_SFF_ID_BITS     11
#define CAN_EFF_ID_BITS     29

/* special address description flags for the CAN_ID */
#define CAN_EFF_FLAG 0x80000000U /* EFF/SFF is set in the MSB */
#define CAN_RTR_FLAG 0x40000000U /* remote transmission request */
#define CAN_ERR_FLAG 0x20000000U /* error message frame */

/* valid bits in CAN ID for frame formats */
#define CAN_SFF_MASK 0x000007FFU /* standard frame format (SFF) */
#define CAN_EFF_MASK 0x1FFFFFFFU /* extended frame format (EFF) */
#define CAN_ERR_MASK 0x1FFFFFFFU /* omit EFF, RTR, ERR flags */

/* Frame type */
#define STANDARD_FRAME  0 /* standard frame */
#define EXTEND_FRAME    1 /* extended frame */

/*
 * Controller Area Network Identifier structure
 *
 * bit 0-28 : CAN identifier (11/29 bit)
 * bit 29   : error message frame flag (0 = data frame, 1 = error message)
 * bit 30   : remote transmission request flag (1 = rtr frame)
 * bit 31   : frame format flag (0 = standard 11 bit, 1 = extended 29 bit)
 */

#define min_t(type, x, y) ({            \
    type __min1 = (x);          \
    type __min2 = (y);          \
    __min1 < __min2 ? __min1: __min2; })

/*
 * get_can_dlc(value) - helper macro to cast a given data length code (dlc)
 * to __u8 and ensure the dlc value to be max. 8 bytes.
 *
 * To be used in the CAN netdriver receive path to ensure conformance with
 * ISO 11898-1 Chapter 8.4.2.3 (DLC field)
 */
#define get_can_dlc(i)      (min_t(UINT8, (i), CAN_MAX_DLC))


/***ft CAN REGISTER offset*/
#define FTCAN_CTRL_OFFSET          0x00 /* Global control register */
#define FTCAN_INTR_OFFSET          0x04 /* Interrupt register */
#define FTCAN_ARB_RATE_CTRL_OFFSET 0x08 /* Arbitration rate control register */
#define FTCAN_DAT_RATE_CTRL_OFFSET 0x0C /* Data rate control register */
#define FTCAN_ACC_ID0_OFFSET       0x10 /* Acceptance identifier0 register */
#define FTCAN_ACC_ID1_OFFSET       0x14 /* Acceptance identifier1 register */
#define FTCAN_ACC_ID2_OFFSET       0x18 /* Acceptance identifier2 register */
#define FTCAN_ACC_ID3_OFFSET       0x1C /* Acceptance identifier3 register */
#define FTCAN_ACC_ID0_MASK_OFFSET  0x20 /* Acceptance identifier0 mask register */
#define FTCAN_ACC_ID1_MASK_OFFSET  0x24 /* Acceptance identifier1 mask register */
#define FTCAN_ACC_ID2_MASK_OFFSET  0x28 /* Acceptance identifier2 mask register */
#define FTCAN_ACC_ID3_MASK_OFFSET  0x2C /* Acceptance identifier3 mask register */
#define FTCAN_XFER_STS_OFFSET      0x30 /* Transfer status register */
#define FTCAN_ERR_CNT_OFFSET       0x34 /* Error counter register */
#define FTCAN_FIFO_CNT_OFFSET      0x38 /* FIFO counter register */
#define FTCAN_DMA_CTRL_OFFSET      0x3C /* DMA request control register */
#define FTCAN_TX_FIFO_OFFSET       0x100 /* TX FIFO shadow register */
#define FTCAN_RX_FIFO_OFFSET       0x200 /* RX FIFO shadow register */




/*----------------------------------------------------------------------------*/
/* CAN register bit masks - FTCAN_<REG>_<BIT>_MASK                            */
/*----------------------------------------------------------------------------*/

/* FTCAN_CTRL mask */
#define FTCAN_CTRL_XFER_MASK   (0x1 << 0)  /* RW */ /*Transfer enable*/
#define FTCAN_CTRL_TXREQ_MASK  (0x1 << 1)  /* RW */ /*Transmit request*/
#define FTCAN_CTRL_AIME_MASK   (0x1 << 2)  /* RW */ /*Acceptance identifier mask enable*/

/* FTCAN_INTR mask */
#define FTCAN_INTR_STATUS_MASK (0xFF << 0) /* RO */ /*the interrupt status*/
#define FTCAN_INTR_BOIS_MASK   (0x1 << 0)  /* RO */ /*Bus off interrupt status*/
#define FTCAN_INTR_PWIS_MASK   (0x1 << 1)  /* RO */ /*Passive warning interrupt status*/
#define FTCAN_INTR_PEIS_MASK   (0x1 << 2)  /* RO */ /*Passive error interrupt status*/
#define FTCAN_INTR_RFIS_MASK   (0x1 << 3)  /* RO */ /*RX FIFO full interrupt status*/
#define FTCAN_INTR_TFIS_MASK   (0x1 << 4)  /* RO */ /*TX FIFO empty interrupt status*/
#define FTCAN_INTR_REIS_MASK   (0x1 << 5)  /* RO */ /*RX frame end interrupt status*/
#define FTCAN_INTR_TEIS_MASK   (0x1 << 6)  /* RO */ /*TX frame end interrupt status*/
#define FTCAN_INTR_EIS_MASK    (0x1 << 7)  /* RO */ /*Error interrupt status*/

#define FTCAN_INTR_EN_MASK     (0xFF << 8) /* RO */ /*the interrupt enable*/
#define FTCAN_INTR_BOIE_MASK   (0x1 << 8)  /* RW */ /*Bus off interrupt enable*/
#define FTCAN_INTR_PWIE_MASK   (0x1 << 9)  /* RW */ /*Passive warning interrupt enable*/
#define FTCAN_INTR_PEIE_MASK   (0x1 << 10) /* RW */ /*Passive error interrupt enable*/
#define FTCAN_INTR_RFIE_MASK   (0x1 << 11) /* RW */ /*RX FIFO full interrupt enable*/
#define FTCAN_INTR_TFIE_MASK   (0x1 << 12) /* RW */ /*TX FIFO empty interrupt enable*/
#define FTCAN_INTR_REIE_MASK   (0x1 << 13) /* RW */ /*RX frame end interrupt enable*/
#define FTCAN_INTR_TEIE_MASK   (0x1 << 14) /* RW */ /*TX frame end interrupt enable*/
#define FTCAN_INTR_EIE_MASK    (0x1 << 15) /* RW */ /*Error interrupt enable*/

#define FTCAN_INTR_BOIC_MASK   (0x1 << 16) /* WO */ /*Bus off interrupt clear*/
#define FTCAN_INTR_PWIC_MASK   (0x1 << 17) /* WO */ /*Passive warning interrupt clear*/
#define FTCAN_INTR_PEIC_MASK   (0x1 << 18) /* WO */ /*Passive error interrupt clear*/
#define FTCAN_INTR_RFIC_MASK   (0x1 << 19) /* WO */ /*RX FIFO full interrupt clear*/
#define FTCAN_INTR_TFIC_MASK   (0x1 << 20) /* WO */ /*TX FIFO empty interrupt clear*/
#define FTCAN_INTR_REIC_MASK   (0x1 << 21) /* WO */ /*RX frame end interrupt clear*/
#define FTCAN_INTR_TEIC_MASK   (0x1 << 22) /* WO */ /*TX frame end interrupt clear*/
#define FTCAN_INTR_EIC_MASK    (0x1 << 23) /* WO */ /*Error interrupt clear*/

/* FTCAN_ACC_ID(0-3)_MASK mask */
#define FTCAN_ACC_IDN_MASK      0x1FFFFFFF /* WO */ /*don’t care the matching */
/* FTCAN_DAT_RATE_CTRL mask */


/* FTCAN_ERR_CNT_OFFSET mask */
#define FTCAN_ERR_CNT_RFN_MASK (0xFF << 0) /* RO */ /*Receive error counter*/
#define FTCAN_ERR_CNT_TFN_MASK (0xFF << 16)/* RO */ /*Transmit error counter*/

/* FTCAN_FIFO_CNT_OFFSET mask */
#define FTCAN_FIFO_CNT_RFN_MASK (0xFF << 0) /* RO */ /*Receive FIFO valid data number*/
#define FTCAN_FIFO_CNT_TFN_MASK (0xFF << 16)/* RO */ /*Transmit FIFO valid data number*/



#define FTCAN_ERR_CNT_TFN_SHIFT     16  /* Tx Error Count shift */
#define FTCAN_FIFO_CNT_TFN_SHIFT    16  /* Tx FIFO Count shift*/
#define FTCAN_IDR_ID1_SHIFT         21  /* Standard Messg Identifier */
#define FTCAN_IDR_ID2_SHIFT         1   /* Extended Message Identifier */
#define FTCAN_IDR_SDLC_SHIFT        14
#define FTCAN_IDR_EDLC_SHIFT        26
#define FTCAN_ACC_IDN_SHIFT         18  /*Standard ACC ID shift*/


#define FTCAN_IDR_ID2_MASK          0x0007FFFE /* Extended message ident */
#define FTCAN_IDR_ID1_MASK          0xFFE00000 /* Standard msg identifier */
#define FTCAN_IDR_IDE_MASK          0x00080000 /* Identifier extension */
#define FTCAN_IDR_SRR_MASK          0x00100000 /* Substitute remote TXreq */
#define FTCAN_IDR_RTR_MASK          0x00000001 /* Extended frames remote TX request */
#define FTCAN_IDR_DLC_MASK          0x0003C000 /* Standard msg dlc */
#define FTCAN_IDR_PAD_MASK          0x00003FFF /* Standard msg padding 1 */
#define FTCAN_IDR_EDLC_MASK         0x3C000000 /* Extended msg dlc */

/*#define FTCAN_INTR_EN     (FTCAN_INTR_TEIE_MASK | FTCAN_INTR_BOIE_MASK |\
                 FTCAN_INTR_RFIE_MASK | FTCAN_INTR_EIE_MASK | \
                 FTCAN_INTR_REIE_MASK)*/

#define FTCAN_INTR_EN       (FTCAN_INTR_TEIE_MASK | FTCAN_INTR_REIE_MASK)
#define FTCAN_INTR_DIS      0x00000000

#define FTCAN_FIFOFRAME_SIZE 4
#define FTCAN_TX_FIFO_MAX   0x40

#define CAN_CALC_MAX_ERROR   50 /* in one-tenth of a percent */
#define CAN_CALC_SYNC_SEG    1
#define clamp(x, low, high)  (min(max(low, x), high))
#define CAN_CLK_FREQ         600000000

#define FWOHCI_CPU_TO_BE32(x) (htobe32((uint32_t)x))
#define cpu_to_be32p(x) (htobe32((uint32_t)x))
#define be32_to_cpup(x) (be32toh((uint32_t)x))

#define DMA_CACHE_DRV_VIRT_TO_PHYS(adrs)    \
    CACHE_DRV_VIRT_TO_PHYS(&cacheDmaFuncs, (void *) (adrs))
    
/*
 * CAN bit-timing parameters
 *
 * For further information, please read chapter "8 BIT TIMING
 * REQUIREMENTS" of the "Bosch CAN Specification version 2.0"
 * at http://www.semiconductors.bosch.de/pdf/can2spec.pdf.
 */
typedef struct can_bittiming {
    UINT bitrate;       /* Bit-rate in bits/second */
    UINT sample_point;  /* Sample point in one-tenth of a percent */
    UINT tq;        /* Time quanta (TQ) in nanoseconds */
    UINT prop_seg;      /* Propagation segment in TQs */
    UINT phase_seg1;    /* Phase buffer segment 1 in TQs */
    UINT phase_seg2;    /* Phase buffer segment 2 in TQs */
    UINT sjw;       /* Synchronisation jump width in TQs */
    UINT brp;       /* Bit-rate prescaler */
} FTCAN_BITTIMING;

/*
 * CAN harware-dependent bit-timing constant
 *
 * Used for calculating and checking bit-timing parameters
 */
typedef struct can_bittiming_const {
    char name[16];      /* Name of the CAN controller hardware */
    UINT tseg1_min; /* Time segement 1 = prop_seg + phase_seg1 */
    UINT tseg1_max;
    UINT tseg2_min; /* Time segement 2 = phase_seg2 */
    UINT tseg2_max;
    UINT sjw_max;       /* Synchronisation jump width */
    UINT brp_min;       /* Bit-rate prescaler */
    UINT brp_max;
    UINT brp_inc;
} FTCAN_BITTIMING_CONST;

typedef struct can_frame {
    UINT can_id;  /* 32 bit CAN_ID + EFF/RTR/ERR flags */
    UINT8    can_dlc; /* frame payload length in byte (0 .. CAN_MAX_DLEN) */
    UINT8    data[CAN_MAX_DLEN];
} FTCAN_FRAME;
    
typedef struct ftCan_drv_ctrl
    {
    VXB_DEVICE_ID   ftCanDev;
    void *      ftCanHandle;
    void *      ftCanRegbase;
    UINT        irq;
    UINT        txCfCnt;
    UINT        txCfMax;
    FTCAN_BITTIMING ftcan_bittiming;
    MSG_Q_ID  ftcanQueue;
    void (*ftCanRecvRtn)(FTCAN_FRAME* pFtCanFrame);
    SEM_ID canTxSem;
    } FTCAN_DRV_CTRL;


#define FTCAN_BASE(p)   ((FTCAN_DRV_CTRL *)(p)->pDrvCtrl)->ftCanRegbase
#define FTCAN_HANDLE(p)   ((FTCAN_DRV_CTRL *)(p)->pDrvCtrl)->ftCanHandle

#define CSR_READ_4(pDev, addr)                                  \
    vxbRead32 (FTCAN_HANDLE(pDev), (UINT32 *)((char *)FTCAN_BASE(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)                           \
    vxbWrite32 (FTCAN_HANDLE(pDev),                             \
        (UINT32 *)((char *)FTCAN_BASE(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (UINT32)(~(val)))
    
    

#define CSR_READ_2(pDev, addr)                                  \
    vxbRead16 (FTCAN_HANDLE(pDev),              \
        (UINT16 *)((char *)FTCAN_BASE(pDev) + addr))

#define CSR_WRITE_2(pDev, addr, data)               \
    vxbWrite16 (FTCAN_HANDLE(pDev),             \
        (UINT16 *)((char *)FTCAN_BASE(pDev) + addr), data)

#define CSR_READ_1(pDev, addr)                                  \
    vxbRead8 (FTCAN_HANDLE(pDev),               \
        ((UINT8 *)FTCAN_BASE(pDev) + addr))

#define CSR_WRITE_1(pDev, addr, data)               \
    vxbWrite8 (FTCAN_HANDLE(pDev),              \
        ((UINT8 *)FTCAN_BASE(pDev) + addr), data)

STATUS ftCanRecvCallback
    (
    unsigned char ctlNo,
    void (*ftCanRecvRtn)(FTCAN_FRAME* pFtCanFrame)
    );
STATUS ftCanSend
    (
    unsigned char ctlNo,
    char *       sendbuff,
    int          lenth
    );
#ifdef __cplusplus
}
#endif

#endif /*end of __INCvxbftcanh*/
