/* vxbArmGenIntCtlrV3.h - ARM Generic interrupt controller driver  */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#ifndef __INCvxbArmGenIntCtlrV3h
#define __INCvxbArmGenIntCtlrV3h

#ifdef __cplusplus
extern "C"{
#endif

#define GIC_VERSION_DEFAULT         2
#define GIC_VERSION_GIC400          3

/*
 * the explanation of some abbreviations
 *
 * CPC - Cross Processor Call
 * IPI - Inter Processor Interrupt
 * GIC - General Interrupt Controller
 * SGI - Software Generated Interrupt
 * PPI - Private Peripheral Interrupt
 * SPI - Shared Peripheral Interrupt
 */

/* MPCore GIC interrupt levels */

#define INT_LVL_MPCORE_IPI00         0 /* This is vxWorks CPC IPI */
#define INT_LVL_MPCORE_CPC           INT_LVL_MPCORE_IPI00

#define INT_LVL_MPCORE_IPI01         1 /* This is vxWorks Debug IPI */
#define INT_LVL_MPCORE_DEBUG         INT_LVL_MPCORE_IPI01

#define INT_LVL_MPCORE_IPI02         2 /* This is vxWorks Reschedule IPI */
#define INT_LVL_MPCORE_SCHED         INT_LVL_MPCORE_IPI02

#define INT_LVL_MPCORE_IPI03         3 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI04         4 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI05         5 /* IPI -> use SWI register in GIC */

#define INT_LVL_MPCORE_IPI06         6 /* This is vxWorks Core Reset IPI */
#define INT_LVL_MPCORE_RESET         INT_LVL_MPCORE_IPI06

#define INT_LVL_MPCORE_IPI07         7 /* This is vxWorks Core Start IPI */
#define INT_LVL_MPCORE_START         INT_LVL_MPCORE_IPI07

#define INT_LVL_MPCORE_IPI08         8 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI09         9 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI10        10 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI11        11 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI12        12 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI13        13 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI14        14 /* IPI -> use SWI register in GIC */
#define INT_LVL_MPCORE_IPI15        15 /* IPI -> use SWI register in GIC */


#define SGI_INT_MAX             16
#define ARM_GIC_IPI_COUNT       16    /* MPCore IPI count         */
#define SPI_START_INT_NUM       32    /* SPI start at ID32        */
#define PPI_START_INT_NUM       16    /* PPI start at ID16        */
#define GIC_INT_MAX_NUM         1020  /* GIC max interrupts count */

#define GIC_INT_ALL_ENABLED     0xFF  /* priority 0-0xFF can run */
#define GIC_INT_ALL_DISABLED    0x00  /* nothing higher than 0 hence disabled */
#define GIC_INT_SPURIOUS        0x3FF /* no interrupt currently */
#define GIC_INT_ONEMINUS_HIGH   0x55555555 /* interrupt config to 1-N, High */
#define GIC_CPU_BINP_DEFAULT    0x07  /* split all priority to subpriority */
#define GIC_CPU_BINP_PREEMPT_VAL    0x00  /* split all priority to group-priority */
#define GIC_CPU_BINP_NONPREEMPT_VAL 0x07  /* split all priority to sub-priority */
#define GIC_CPU_CONTROL_ENABLE  0x1   /* enable the processor interface */
#define GIC_CPU_ALL_ENABLED     0xFF  /* priority 0-E can run */
#define GIC_CPU_ALL_DISABLED    0x00  /* nothing higher than 0 */
#define GIC_SGI_SRC_CPU_ID_MASK 0x1C00
#define GIC_INT_HIGHEST_PRIORITY 0x0  /* the highest priority for interrupts */
#define GIC_INT_LOWEST_PRIORITY  0x1F /* the lowest priority for interrupts */
#define GIC_INT_PRIORITY_SHIFT   0x8
#define GIC_INT_PRIORITY_MASK    0xFF
#define GIC_INT_TRIGGER_SHIFT    0x2

#define GIC_CPU_DIR_DEFAULT 0x01010101 /* all interrupts are directed to CPU0 */
#define GIC_V3_CPU_DIR_DEFAULT      0x0llu /* all interrupts are directed to CPU0 */
#define GIC_CONTROL_ENABLE  0x01

#define ALL_PPI_INT_MASK  0xFFFF0000 /* bit field for all PPI interrupts */
#define ALL_SGI_INT_MASK  0x0000FFFF /* bit field for all SGI interrupts */

#define GIC_AFF_ALL_MASK            0xff00ffffff
#define GIC_AFF_LOW32_MASK          0x00ffffff
/* ARM GIC interrupt distributor and CPU interface register access macros */

#define SHIFT_PER_WORD              5
#define BITS_PER_WORD           32
#define CONFIGS_PER_WORD        16
#define PRIOS_PER_WORD          4
#define TARGETS_PER_WORD        4
#define NWORD(bitnum)           (bitnum / 32)
#define BIT(bitnum)             (1 << (bitnum % BITS_PER_WORD))

/* trigger modes */

#define GIC_TRIG_RISING_EDGE       (0x1)
#define GIC_TRIG_FALLING_EDGE      (0x2)
#define GIC_TRIG_ACTIVE_HIGH       (0x4)
#define GIC_TRIG_ACTIVE_LOW        (0x8)

/* SGI & PPI  configuration  for GIC 400 */

#define GIC_SGI_OFF     (1 << 16)
#define GICR_ICFGR0     (0x0C00) /* SGI  & PPI Configuration Register */
#define GICR_ICFGR1     (0x0C04) /* PPI Configuration Register */
#define GICR_ISENABLER0 (0x100)
#define GICR_ICENABLER0 (0x180)

/* SGI Configuration Register */

#define IMR              (1 << (40 - 32))
#define ipiId_MASK       (0xffff)

#define GICR_WAKER_CPU(cpu)     ((volatile UINT32 *)(GIC_REDIST + cpu*(2<<16) + 0x14))
#define GICR_IntEnable_CPU(cpu)   ((volatile UINT32 *)(GIC_SGI_BASE + cpu*(2<<16) +0x100))
#define GICR_IntEnClr_CPU(cpu)    ((volatile UINT32 *)(GIC_SGI_BASE + cpu*(2<<16) + 0x180))
#define GICR_IntPendClr_CPU(cpu)  ((volatile UINT32 *)(GIC_SGI_BASE + cpu*(2<<16) + 0x280))
#define GICR_Prio_CPU(cpu,Periph)       ((volatile UINT32 *)(GIC_SGI_BASE+ cpu*(2<<16)  + 0x400 + \
                                 (0x4 * (Periph / PRIOS_PER_WORD))))

#define GICR_WAKER     ((volatile UINT32 *)(GIC_REDIST + 0x14))
#define GICR_TYPERLO     ((volatile UINT32 *)(GIC_REDIST + 0x8))
#define GICR_TYPERHI     ((volatile UINT32 *)(GIC_REDIST + 0xC))

#define GICR_Config0     ((volatile UINT32 *)(GIC_SGI_BASE + 0xC00))

/* PPI Configuration Register */

#define GICR_Config1     ((volatile UINT32 *)(GIC_SGI_BASE + 0xC04))

/*Interrupt Group Register 0*/

#define GICR_IGROUPR0  ((volatile UINT32 *)(GIC_SGI_BASE + 0x80))
/* interrupt enable */

#define GICR_IntEnable   ((volatile UINT32 *)(GIC_SGI_BASE + 0x100))

/* clear interrupt enable */

#define GICR_IntEnClr    ((volatile UINT32 *)(GIC_SGI_BASE + 0x180))

/* set pending interrupt */

#define GICR_IntPendSet  ((volatile UINT32 *)(GIC_SGI_BASE + 0x200))

/* clear pending interrupt */

#define GICR_IntPendClr  ((volatile UINT32 *)(GIC_SGI_BASE + 0x280))

/* interrupt priority  */

#define GICR_Prio(Periph)       ((volatile UINT32 *)(GIC_SGI_BASE + 0x400 + \
                                 (0x4 * (Periph / PRIOS_PER_WORD))))
#ifndef _ASMLANGUAGE

/* GIC distributor and CPU interface register base offsets */

#define GIC_REDIST    (armGicBase + armGicReDistOffset)
#define GIC_SGI_BASE  (armGicBase + armGicReDistOffset+ GIC_SGI_OFF)
#define GIC_DIST  (armGicBase + armGicDistOffset)
#ifdef  SUPPORT_GICC
#define GIC_CPU   (armGicBase + armGicCpuOffset)
#endif
/* interrupt distributor control */

#define GIC_Control            ((volatile UINT32 *)(GIC_DIST + 0x000))

/* interrupt distributor control */

#define GIC_SET_SPI            ((volatile UINT32 *)(GIC_DIST + 0x040))

#define GIC_CL_SPI             ((volatile UINT32 *)(GIC_DIST + 0x048))

/* interrupt controller type */

#define GIC_Type               ((volatile UINT32 *)(GIC_DIST + 0x004))

/* interrupt enable */

#define GIC_IntEnable(Periph)  ((volatile UINT32 *)(GIC_DIST + 0x100 + \
                                          (0x4 * NWORD(Periph))))
/* clear interrupt enable */

#define GIC_IntEnClr(Periph)   ((volatile UINT32 *)(GIC_DIST + 0x180 + \
                                          (0x4 * NWORD(Periph))))
/* interrupt active status */

#define GIC_IntActSts(Periph)   ((volatile UINT32 *)(GIC_DIST + 0x300 + \
                                          (0x4 * NWORD(Periph))))
/* set pending interrupt */

#define GIC_IntPendSet(Periph) ((volatile UINT32 *)(GIC_DIST + 0x200 + \
                                          (0x4 * NWORD(Periph))))                                         

/* clear pending interrupt */

#define GIC_IntPendClr(Periph) ((volatile UINT32 *)(GIC_DIST + 0x280 + \
                                          (0x4 * NWORD(Periph))))
/* interrupt configuration */

#define GIC_Config(Periph)     ((volatile UINT32 *)(GIC_DIST + 0xC00 + \
                                          (0x4 * (Periph / CONFIGS_PER_WORD))))
/* interrupt priority  */

#define GIC_Prio(Periph)       ((volatile UINT32 *)(GIC_DIST + 0x400 + \
                                          (0x4 * (Periph / PRIOS_PER_WORD))))
/* CPU target */

#define GIC_CPUTarg(Periph)    ((volatile UINT32 *)(GIC_DIST + 0x800 + \
                                          (0x4 * (Periph / TARGETS_PER_WORD))))

/* Interrupt Routing */

#define GIC_IntRpute(Periph)   ((volatile UINT64 *)(GIC_DIST + 0x6000 + \
                                          (0x8 * (Periph))))


/* software interrupt generate */

#define GIC_SWInterrupt        ((volatile UINT32 *)(GIC_DIST + 0xF00))

/* CPU interrupt control */
#ifdef  SUPPORT_GICC
#define GIC_CPU_Control        ((volatile UINT32 *)(GIC_CPU + 0x00))

/* CPU priority mask */

#define GIC_CPU_PriMask        ((volatile UINT32 *)(GIC_CPU + 0x04))

/* CPU binary point */

#define GIC_CPU_BinPoint       ((volatile UINT32 *)(GIC_CPU + 0x08))

/* CPU interrupt acknowledge */

#define GIC_CPU_IntAck         ((volatile UINT32 *)(GIC_CPU + 0x0C))

/* CPU end of interrupt */

#define GIC_CPU_EOInt          ((volatile UINT32 *)(GIC_CPU + 0x10))

/* CPU running priority */

#define GIC_CPU_RunPri         ((volatile UINT32 *)(GIC_CPU + 0x14))
#endif

/* --------------D2000 special hw------------- */
#define IXIC_CTR_BASE_DEFAULT 0x29000000
#define IXIC_HPB_BASE_DEFAULT 0x29100000

#define CTR_BANK_NUM        6
#define CTR_BANK_SIZE       0x10000
#define CTR_BANK_ISTATUS_LOCAL  0x184

#define HPB_INTX_STATUS_0   0x0
#define HPB_INTX_STATUS_1   0x1000

#define PHYTIUM_PCIE_ISTATUS_LOCAL  0x184
#define PHYTIUM_PEU0_C0         0x0
#define PHYTIUM_PEU0_C1         0x10000
#define PHYTIUM_PEU0_C2         0x20000
#define PHYTIUM_PEU1_C0         0x30000
#define PHYTIUM_PEU1_C1         0x40000
#define PHYTIUM_PEU1_C2         0x50000
#define PHYTIUM_PEU0_INTX_STATUS    0x100000
#define PHYTIUM_PEU1_INTX_STATUS    0x101000
/* ------------------------------------------- */


#endif /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif  /* __INCvxbArmGenIntCtlrV3h */
