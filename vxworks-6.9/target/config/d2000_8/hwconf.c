/* hwconf.c - Hardware configuration support module */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#include <vxWorks.h>
#include <vsbConfig.h>
#include <vxBusLib.h>
#include <hwif/vxbus/vxBus.h>
#include <hwif/vxbus/vxbIntrCtlr.h>
#include <hwif/util/vxbParamSys.h>
#include <hwif/vxbus/hwConf.h>
#include "config.h"
#include "soc.h"
#ifdef DRV_FTI2C
#include <hwif/vxbus/vxbI2cLib.h>
#endif
#ifdef DRV_FTQSPI
#include "vxbFtQspi.h"
#endif
#ifdef DRV_PCIBUS_FT
#   include <drv/pci/pciAutoConfigLib.h>
#endif


#define GEN_TIMER_IRQ                   30
#define AUX_GEN_TIMER_IRQ               27

#define SYS_CLK_REG_BASE                0xeeee0000
#define SYS_CLK_FREQ                    50000000

#define AUX_CLK_REG_BASE                0xeeef0000
#define AUX_CLK_FREQ                    50000000


#ifdef  DRV_ARM_GICV3

LOCAL struct intrCtlrInputs gicInputs[] = {
    /* pin,                driver,            unit,   index */
   { GEN_TIMER_IRQ,                 "armGenTimer",       0,       0 }, 
   
#ifdef  DRV_ARM_GEN_AUX_TIMER    
    { AUX_GEN_TIMER_IRQ,       "armAuxTimer",    0,       0 }, 
#endif
    { INT_VEC_UART0INTR,       "primeCellSioDev",  0,       0 },   
    { INT_VEC_UART1INTR,       "primeCellSioDev",  1,       0 },  
    { INT_VEC_UART2INTR,       "primeCellSioDev",  2,       0 },   
    { INT_VEC_UART3INTR,       "primeCellSioDev",  3,       0 },
    { 81,        "gmac",              0,       0 },
    { 82,        "gmac",              1,       0 },
    { FTCAN0_IRQ,       "ftCan",  0,       0 },   
    { FTCAN1_IRQ,       "ftCan",  1,       0 },  
    { FTCAN2_IRQ,       "ftCan",  2,       0 },
    { 60,         "legacy",     0,  60 }, /* Peu0 legacy interrupt number : 60,61,62,63*/
    { 61,         "legacy",     0,  61 },
    { 62,         "legacy",     0,  62 },
    { 63,         "legacy",     0,  63 },
    { 52,         "ftSdhci",        0,       0 },
    { 53,         "ftSdhci",        0,       1 },
    { 54,         "ftSdhci",        0,       2 },
    { I2C_0_INTR_IRQ,       "ftI2c",  0,       0 },   
    { I2C_1_INTR_IRQ,       "ftI2c",  1,       0 },  
    { I2C_2_INTR_IRQ,       "ftI2c",  2,       0 },
    { I2C_3_INTR_IRQ,       "ftI2c",  3,       0 },
    { INT_VEC_GPIO0INTR,    "ftGpio", 0,       0 },   
    { INT_VEC_GPIO1INTR,    "ftGpio", 1,       0 },      
    { INT_VEC_GPIO0INTR,    "legacy", 0,  INT_VEC_GPIO0INTR },
    { INT_VEC_GPIO1INTR,    "legacy", 0,  INT_VEC_GPIO1INTR },
    { FTSCPI_IRQ,           "ftScpi", 0,       0 },
};

LOCAL const struct intrCtlrPriority gicPriority[] = {
    /* pin,                priority */
    { GEN_TIMER_IRQ,           0x0  }, 
   
#ifdef  DRV_ARM_GEN_AUX_TIMER    
    { AUX_GEN_TIMER_IRQ,       0x0  }, 
#endif
    { INT_VEC_UART0INTR,       0x10 },   
    { INT_VEC_UART1INTR,       0x10 },  
    { INT_VEC_UART2INTR,       0x10 },   
    { INT_VEC_UART3INTR,       0x10 },
    { 81,                      0x10 }, /*gmac0*/
    { 82,                      0x10 }, /*gmac1*/
    { FTCAN0_IRQ,              0x10 },   
    { FTCAN1_IRQ,              0x10 },  
    { FTCAN2_IRQ,              0x10 },
    { 60,                      0x10 }, /* Peu0 legacy interrupt number : 60,61,62,63*/
    { 61,                      0x10 },
    { 62,                      0x10 },
    { 63,                      0x10 },
    { 52,                      0x20 }, /*ftSdhci interrupt number : 52,53,54*/
    { 53,                      0x20 },
    { 54,                      0x20 },
    { I2C_0_INTR_IRQ,          0x20 },   
    { I2C_1_INTR_IRQ,          0x20 },  
    { I2C_2_INTR_IRQ,          0x20 },
    { I2C_3_INTR_IRQ,          0x20 },
    { INT_VEC_GPIO0INTR,       0x20 },   
    { INT_VEC_GPIO1INTR,       0x20 },
};

#ifdef _WRS_CONFIG_SMP
struct intrCtlrCpu gicCpu[] = {
    /* pin,                CPU */
    { INT_VEC_UART0INTR,       0 },   
    { INT_VEC_UART1INTR,       0 },  
    { INT_VEC_UART2INTR,       0 },   
    { INT_VEC_UART3INTR,       0 },
    { 81,                      7 }, /*gmac0*/
    { 82,                      0 }, /*gmac1*/
    { FTCAN0_IRQ,              0 },   
    { FTCAN1_IRQ,              0 },  
    { FTCAN2_IRQ,              0 },
    { 60,                      0 }, /* Peu0 legacy interrupt number : 60,61,62,63*/
    { 61,                      0 },
    { 62,                      0 },
    { 63,                      0 },
    { 52,                      0 }, /*ftSdhci interrupt number : 52,53,54*/
    { 53,                      0 },
    { 54,                      0 },
    { I2C_0_INTR_IRQ,          0 },   
    { I2C_1_INTR_IRQ,          6 },  
    { I2C_2_INTR_IRQ,          0 },
    { I2C_3_INTR_IRQ,          0 },
    { INT_VEC_GPIO0INTR,       0 },   
    { INT_VEC_GPIO1INTR,       0 }, 
};
#endif /* _WRS_CONFIG_SMP */

LOCAL const struct intrCtlrTrigger gicTrigger[] = {
    /* pin,                     sensitivity */

    { INT_VEC_GPIO0INTR,            VXB_INTR_TRIG_RISING_EDGE },
    { INT_VEC_GPIO1INTR,            VXB_INTR_TRIG_RISING_EDGE },
};

/* dts info: 
interrupt-controller;
    reg = <0x0 0x29a00000 0 0x20000>,    GICD 
          <0x0 0x29b00000 0 0x100000>,   GICR 
          <0x0 0x29c00000 0 0x10000>,    GICC 
          <0x0 0x29c10000 0 0x10000>,    GICH 
          <0x0 0x29c20000 0 0x10000>;    GICV 
*/

/*
 * interrupt mode - interrupts can be in either preemptive or non-preemptive
 * mode. For preemptive mode, change INT_MODE to INT_PREEMPT_MODEL
 */

#define INT_MODE                 INT_NON_PREEMPT_MODEL
LOCAL const struct hcfResource armGICv3Resources[] = {
    { "regBase",           HCF_RES_INT, { (void *)GIC_BASE_ADR } },
    { "distOffset",        HCF_RES_INT,  {(void *)0 } },
    { "redistOffset",      HCF_RES_INT,  {(void *)0x100000 } },
    { "input",             HCF_RES_ADDR, {(void *)&gicInputs[0] } },
#ifdef _WRS_CONFIG_SMP
    { "cpuRoute",          HCF_RES_ADDR, {(void *)&gicCpu[0] } },
    { "cpuRouteTableSize", HCF_RES_INT,  {(void *)NELEMENTS(gicCpu) } },
#endif
    { "inputTableSize",    HCF_RES_INT,  {(void *)NELEMENTS(gicInputs) } },
    { "priority",          HCF_RES_ADDR, {(void *)&gicPriority[0]} },
    { "priorityTableSize", HCF_RES_INT,  {(void *)NELEMENTS(gicPriority) } },
    { "trigger",           HCF_RES_ADDR, {(void *)&gicTrigger[0]} },
    { "triggerTableSize",  HCF_RES_INT,  {(void *)NELEMENTS(gicTrigger) } },
    { "intMode",           HCF_RES_INT,  {(void *)INT_MODE } },
#ifdef _WRS_CONFIG_SMP
    { "maxCpuNum",         HCF_RES_INT,  {(void *)8} },
#endif /* _WRS_CONFIG_SMP */
     /* D2000 uses CTR&HPB */
    { "ixicCTR",        HCF_RES_INT,  {(void *)0x29000000 } },
    { "ixicHPB",        HCF_RES_INT,  {(void *)0x29100000 } },
};
#define armGICv3Num NELEMENTS(armGICv3Resources)
#endif  /* DRV_ARM_GICV3 */


struct hcfResource primeCellSioDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART_0_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)SOC_UART_CLK} },
};
#define primeCellSioDev0Num NELEMENTS(primeCellSioDev0Resources)

struct hcfResource primeCellSioDev1Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART_1_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)SOC_UART_CLK} },
};
#define primeCellSioDev1Num NELEMENTS(primeCellSioDev1Resources)

struct hcfResource primeCellSioDev2Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART_2_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)SOC_UART_CLK} },
};
#define primeCellSioDev2Num NELEMENTS(primeCellSioDev2Resources)

struct hcfResource primeCellSioDev3Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)UART_3_BASE_ADR} },
    { "clkFreq", HCF_RES_INT, {(void *)SOC_UART_CLK} },
};
#define primeCellSioDev3Num NELEMENTS(primeCellSioDev3Resources)

struct hcfResource ftCanDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)FTCAN0_BASE} },
    { "irq", HCF_RES_INT, {(void *)FTCAN0_IRQ} },
    { "bitrate", HCF_RES_INT, {(void *)FTCAN0_BITRATE} },
};
#define ftCanDev0Num NELEMENTS(ftCanDev0Resources)

struct hcfResource ftCanDev1Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)FTCAN1_BASE} },
    { "irq", HCF_RES_INT, {(void *)FTCAN1_IRQ} },
    { "bitrate", HCF_RES_INT, {(void *)FTCAN1_BITRATE} },
};
#define ftCanDev1Num NELEMENTS(ftCanDev1Resources)

struct hcfResource ftCanDev2Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)FTCAN2_BASE} },
    { "irq", HCF_RES_INT, {(void *)FTCAN2_IRQ} },
    { "bitrate", HCF_RES_INT, {(void *)FTCAN2_BITRATE} },
};
#define ftCanDev2Num NELEMENTS(ftCanDev2Resources)

#ifdef DRV_FTSCPI
struct hcfResource ftScpiDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)FTSCPI_BASE} },
    { "rxMemBase", HCF_RES_INT, {(void *)FTSCPI_RX_MEM_BASE} },
    { "txMemBase", HCF_RES_INT, {(void *)FTSCPI_TX_MEM_BASE} },
};
#define ftScpiDev0Num NELEMENTS(ftScpiDev0Resources)
#endif

#ifdef DRV_FTQSPI
struct vxbSpiDevInfo spiDevTbl[] = {
    /* name                     cs      width   freq        mode */
#ifdef  DRV_SPIFLASH_SP25   
    { SPI_FLASH_DEVICE_NAME,     0,       8,   30000000,      1},
#endif     
};

struct hcfResource qspiResources[] =  {
    { "regBase",       HCF_RES_INT,   { (void *)(0x28014000) } },
    { "capacity",      HCF_RES_INT,   { (void *)(QSPI_FLASH_CAP_32MB) } },    
    { "clkDiv",        HCF_RES_INT,   { (void *)(QSPI_SCK_DIV_128) } },
    { "transMode",     HCF_RES_INT,   { (void *)(QSPI_TRANSFER_1_1_1) } },
    { "addrMode",      HCF_RES_INT,   { (void *)(QSPI_ADDR_SEL_3) } },  
    { "spiDev",        HCF_RES_ADDR,  { (void *)&spiDevTbl[0]} },
    { "spiDevNum",     HCF_RES_INT,   { (void *)NELEMENTS(spiDevTbl)}},

};
#   define qspiNum  NELEMENTS(qspiResources)
#endif /* DRV_FTQSPI */


#ifdef DRV_FTI2C
LOCAL struct i2cDevInputs i2cDev0Input[] = {
        /* Name */   /* Addr (7-bit) */   /* flag */
#ifdef  DRV_I2C_EEPROM
        { "eeprom_at24c32",    (0x57),    I2C_WORDADDR },
#endif
};
struct hcfResource i2cDev0Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)REG_I2C0_BASE} },
    { "irq", HCF_RES_INT, {(void *)I2C_0_INTR_IRQ} },
    { "clkFreq", HCF_RES_INT,{(void *)SOC_I2C_CLK} },
    { "busSpeed", HCF_RES_INT,{(void *)400000} },
    { "polling", HCF_RES_INT,{(void *)TRUE} },
    { "i2cDev",  HCF_RES_ADDR, {(void *)&i2cDev0Input[0] } },
    { "i2cDevNum", HCF_RES_INT,{(void *)NELEMENTS(i2cDev0Input)} },

    /* I2C0 pin default is I2C0. Need not DeMux.
    { "pinDemuxReg", HCF_RES_INT,{(void *)0} },
    { "pinDemuxBit", HCF_RES_INT,{(void *)0} }, 
    */
};
#define i2cDev0Num NELEMENTS(i2cDev0Resources)

LOCAL struct i2cDevInputs i2cDev1Input[] = {
       /* Name */   /* Addr (7-bit) */   /* flag */
#ifdef DRV_I2C_RTC  
        { "rtc_ds1339",    (0xD0>>1),    0 },
#endif
};
struct hcfResource i2cDev1Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)REG_I2C1_BASE} },
    { "irq", HCF_RES_INT, {(void *)I2C_1_INTR_IRQ} },
    { "clkFreq", HCF_RES_INT,{(void *)SOC_I2C_CLK} },
    { "busSpeed", HCF_RES_INT,{(void *)400000} },
    { "polling", HCF_RES_INT,{(void *)TRUE} },
    { "i2cDev",  HCF_RES_ADDR, {(void *)&i2cDev1Input[0] } },
    { "i2cDevNum", HCF_RES_INT,{(void *)NELEMENTS(i2cDev1Input)} },
    { "pinDemuxReg", HCF_RES_INT,{(void *)0x28180200} }, /* pin MUX/DEMUX register */
    { "pinDemuxBit", HCF_RES_INT,{(void *)0x22000000} }, /* pin MUX/DEMUX bit value */
};
#define i2cDev1Num NELEMENTS(i2cDev1Resources)

LOCAL struct i2cDevInputs i2cDev2Input[] = {
        
};
struct hcfResource i2cDev2Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)REG_I2C2_BASE} },
    { "irq", HCF_RES_INT, {(void *)I2C_2_INTR_IRQ} },
    { "clkFreq", HCF_RES_INT,{(void *)SOC_I2C_CLK} },
    { "busSpeed", HCF_RES_INT,{(void *)400000} },
    { "polling", HCF_RES_INT,{(void *)TRUE} },   
    { "i2cDev",  HCF_RES_ADDR, {(void *)&i2cDev2Input[0] } },
    { "i2cDevNum", HCF_RES_INT,{(void *)NELEMENTS(i2cDev2Input)} },  
    { "pinDemuxReg", HCF_RES_INT,{(void *)0x28180204} }, /* pin MUX/DEMUX register */
    { "pinDemuxBit", HCF_RES_INT,{(void *)0x00000220} }, /* pin MUX/DEMUX bit value */
};
#define i2cDev2Num NELEMENTS(i2cDev2Resources)

LOCAL struct i2cDevInputs i2cDev3Input[] = {
        
};
struct hcfResource i2cDev3Resources[] = {
    { "regBase", HCF_RES_INT, {(void *)REG_I2C3_BASE} },
    { "irq", HCF_RES_INT, {(void *)I2C_3_INTR_IRQ} },
    { "clkFreq", HCF_RES_INT,{(void *)SOC_I2C_CLK} },
    { "busSpeed", HCF_RES_INT,{(void *)400000} },
    { "polling", HCF_RES_INT,{(void *)TRUE} },
    { "i2cDev",  HCF_RES_ADDR, {(void *)&i2cDev3Input[0] } },
    { "i2cDevNum", HCF_RES_INT,{(void *)NELEMENTS(i2cDev3Input)} },
    { "pinDemuxReg",   HCF_RES_INT,{(void *)0x28180204} }, /* pin MUX/DEMUX register */
    { "pinDemuxBit",   HCF_RES_INT,{(void *)0x00000002} }, /* pin MUX/DEMUX bit value */
    { "pinDemuxRegEx", HCF_RES_INT,{(void *)0x28180208} }, /* pin MUX/DEMUX register */
    { "pinDemuxBitEx", HCF_RES_INT,{(void *)0x20000000} }, /* pin MUX/DEMUX bit value */
};
#define i2cDev3Num NELEMENTS(i2cDev3Resources)
#endif

#ifdef DRV_FTGPIO
/*This table is used to set the default pin mode of portA*/
LOCAL UINT8 gpio0PortAModeTable[] = { 
/*pin of portA can be used as interrupt mode*/
/*portA-pinX:  0  1  2  3  4  5  6  7  */       
               3, 2, 1, 0, 0, 0, 0, 0   /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                           2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/          
};
/*This table is used to set the default pin mode of portB*/
LOCAL UINT8 gpio0PortBModeTable[] = {  
/*pin of portB can not be used as interrupt mode*/
/*portB-pinX:  0  1  2  3  4  5  6  7  */       
               1, 2, 1, 0, 0, 0, 0, 0        
};
struct hcfResource gpioDev0Resources[] = {
    { "regBase",           HCF_RES_INT,     { (void *)GPIO_0_BASE_ADR } },
    { "portAModeTable",    HCF_RES_ADDR,    { (void *)&gpio0PortAModeTable[0] } },
    { "portAModeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio0PortAModeTable)} },
    { "portBModeTable",    HCF_RES_ADDR,    { (void *)&gpio0PortBModeTable[0] } },
    { "portBModeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio0PortBModeTable)} },
    { "trigger",           HCF_RES_INT,     { (void *)VXB_INTR_TRIG_RISING_EDGE} },    
};
#define gpioDev0Num NELEMENTS(gpioDev0Resources)

LOCAL UINT8 gpio1PortAModeTable[] = {                         
/*portA-pinX:  0  1  2  3  4  5  6  7  */       
               0, 0, 0, 0, 0, 0, 0, 0   /* 0:GPIO_MODE_NOT_USED  1:GPIO_MODE_IN 
                                           2:GPIO_MODE_OUT       3:GPIO_MODE_INT*/          
};
LOCAL UINT8 gpio1PortBModeTable[] = {                         
/*portB-pinX:  0  1  2  3  4  5  6  7  */       
               0, 0, 0, 0, 0, 0, 0, 0        
};
struct hcfResource gpioDev1Resources[] = {
    { "regBase",           HCF_RES_INT,    { (void *)GPIO_1_BASE_ADR } },
    { "portAModeTable",    HCF_RES_ADDR,    { (void *)&gpio1PortAModeTable[0] } },
    { "portAModeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio1PortAModeTable)} },
    { "portBModeTable",    HCF_RES_ADDR,    { (void *)&gpio1PortBModeTable[0] } },
    { "portBModeTableNum", HCF_RES_INT,     { (void *)NELEMENTS(gpio1PortBModeTable)} },
    { "trigger",           HCF_RES_INT,     { (void *)VXB_INTR_TRIG_RISING_EDGE} },
};
#define gpioDev1Num NELEMENTS(gpioDev1Resources)
#endif 

#ifdef  DRV_ARM_GEN_SYS_TIMER
struct hcfResource armGenSysTimerResources[] = {
    { "regBase", HCF_RES_INT, {(void *)0xeeee0000} },
    { "irq",     HCF_RES_INT, {(void *)GEN_TIMER_IRQ} },
    { "clkFreq", HCF_RES_INT, {(void *)SYS_CLK_FREQ} },
    { "minClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MIN} },
    { "maxClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MAX} },
};
#define armGenSysTimerNum NELEMENTS(armGenSysTimerResources)
#endif  /* DRV_ARM_GEN_SYS_TIMER */

#ifdef  DRV_ARM_GEN_AUX_TIMER
struct hcfResource armAuxGenTimerResources[] = {
    { "regBase", HCF_RES_INT, {(void *)AUX_CLK_REG_BASE} },
    { "irq",     HCF_RES_INT, {(void *)AUX_GEN_TIMER_IRQ} },
    { "clkFreq", HCF_RES_INT, {(void *)AUX_CLK_FREQ} },
    { "minClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MIN} },
    { "maxClkRate", HCF_RES_INT, {(void *)SYS_CLK_RATE_MAX} },
};
#define armAuxGenTimerNum NELEMENTS(armAuxGenTimerResources)
#endif  /* DRV_ARM_GEN_SYS_TIMER */

#ifdef DRV_PCIBUS_FT
IMPORT STATUS sysPciExAutoconfigInclude(PCI_SYSTEM *, PCI_LOC *, UINT);
IMPORT UCHAR sysPciExAutoconfigIntrAssign(PCI_SYSTEM *, PCI_LOC *, UCHAR);
IMPORT VOID sysPciPirqEnable (BOOL enable);
const struct hcfResource ftPci0Resources[] = {
    { "regBase",            HCF_RES_INT,  { (void *)(FT_PCI_CONFIG_ADDR) } },
    { "memIo32Addr",        HCF_RES_ADDR, { (void *)0x58000000 } },
    { "memIo32Size",        HCF_RES_INT,  { (void *)0x27f00000 } },
    { "io32Addr",           HCF_RES_ADDR, { (void *)0x50000000 } },
    { "io32Size",           HCF_RES_INT,  { (void *)0x08000000 } },

    { "io16Addr",           HCF_RES_ADDR, { (void *)0x1000 } },
    { "io16Size",           HCF_RES_INT,  { (void *)0x0ffff000 } },

    { "ioStart",            HCF_RES_ADDR,  { (void *)0x50000000 } },

    { "fbbEnable",          HCF_RES_INT,  { (void *)TRUE } },
    { "cacheSize",          HCF_RES_INT,  { (void *)(8) } },
    { "maxLatAllSet",       HCF_RES_INT,  { (void *)0x40 } },
    { "autoIntRouteSet",    HCF_RES_INT,  { (void *)FALSE } },

    { "includeFuncSet",     HCF_RES_ADDR, { (void *)sysPciExAutoconfigInclude } },
    { "intAssignFuncSet",   HCF_RES_ADDR, { (void *)sysPciExAutoconfigIntrAssign } },
    { "funcPirqEnable",     HCF_RES_ADDR, { (void *)sysPciPirqEnable}},
    { "maxBusSet",          HCF_RES_INT,  { (void *)32 } },

    /* Window Attributes - Defaults to 8540 type if none given */

    { "autoConfig",         HCF_RES_INT,  { (void *)(FALSE)} },

#ifdef INCLUDE_INTCTLR_DYNAMIC_LIB
    { "msiEnable",          HCF_RES_INT,  { (void *)(TRUE)} },
    { "dynamicInterrupts",  HCF_RES_INT,  { (void *)(TRUE)} }
#else
    { "msiEnable",          HCF_RES_INT,  { (void *)(FALSE)} },
    { "dynamicInterrupts",  HCF_RES_INT,  { (void *)(FALSE)} }
#endif /* INCLUDE_INTCTLR_DYNAMIC_LIB */
};
#define ftPci0Num NELEMENTS(ftPci0Resources)
#endif /* DRV_PCIBUS_M85XX */

LOCAL struct hcfResource ftSdhc0Resources[] = {
    { "regBase",    HCF_RES_INT,  {(void *)FT_SDHC0_BASE} },
    { "clkFreq",    HCF_RES_ADDR, {(void *)sysSdhcClkFreqGet} },    
    { "polling",    HCF_RES_INT,  {(void *)TRUE} }   /* interrupt mode dose not support*/
};
#define ftSdhc0Num NELEMENTS(ftSdhc0Resources)

#ifdef INCLUDE_PC_CONSOLE
LOCAL const struct hcfResource pentiumM6845VgaResources[] =
    {
        { "colorMode",   HCF_RES_INT, {(void *) 1} },
        { "colorSetting",HCF_RES_INT, {(void *) 0x1f} },
    };
#define pentiumM6845VgaNum NELEMENTS(pentiumM6845VgaResources)
#endif

#ifdef DRV_VXBEND_FTGMAC 

#define PHYADDR_OF_GREEN_BOARD 4 /* for reference green board. hardware V1 */
#define PHYADDR_OF_BLACK_BOARD 3 /* for reference black board. hardware V2 */
#define PHYADDR_OF_AUTO       -1 /* auto probe when start */

struct hcfResource gmac0Resources[] = {
    { "regBase",    HCF_RES_INT, {(void *)0x2820c000} },
    { "phyAddr",    HCF_RES_INT,    {(void *)PHYADDR_OF_AUTO} },
    { "macAddrSet", HCF_RES_ADDR,   {(void *)sysGmacAddrSet} },
    { "miiIfName",  HCF_RES_STRING, {(void *)"gmac"} },
    { "miiIfUnit",  HCF_RES_INT,    {(void *)0} },
    /*
     * The clkMDC resource specifies the CSR clock range. Use clk_csr_i frequency
     * to compute MDC clock frequency. To be sure MDC clock is in [1.0MHz ~2.5MHz] }
     *   Table GMII Address Register
     *   bit[5:2] clk_csr_i   MDC clock
     *   0000    60-100MHz    clk_csr_i
     *   0001   100-150 MHz   clk_csr_i/62
     *   0010   20-35 MHz     clk_csr_i/16
     *   0011   35-60 MHz     clk_csr_i/26
     *   0100   150-250 MHz   clk_csr_i/102
     *   0101   250-300 MHz   clk_csr_i/124   <==
     *   0110, 0111 Reserved
     */
    { "clkMDC",     HCF_RES_INT,    {(void *)0x5} }
};
#define gmac0Num NELEMENTS(gmac0Resources)

struct hcfResource gmac1Resources[] = {
    { "regBase",    HCF_RES_INT, {(void *)0x28210000} },
    { "phyAddr",    HCF_RES_INT,    {(void *)PHYADDR_OF_AUTO} },
    { "macAddrSet", HCF_RES_ADDR,   {(void *)sysGmacAddrSet} },
    { "miiIfName",  HCF_RES_STRING, {(void *)"gmac"} },
    { "miiIfUnit",  HCF_RES_INT,    {(void *)1} },
    { "clkMDC",     HCF_RES_INT,    {(void *)0x5} }
};
#define gmac1Num NELEMENTS(gmac1Resources)
#endif /* DRV_VXBEND_FTGMAC */ 

const struct hcfDevice hcfDeviceList[] = {
#ifdef  DRV_ARM_GICV3
    { "armGicDev", 0, VXB_BUSID_PLB, 0, armGICv3Num, armGICv3Resources},
#endif  /* DRV_ARM_GIC */
   { "primeCellSioDev",  0, VXB_BUSID_PLB, 0, primeCellSioDev0Num , primeCellSioDev0Resources },   
   { "primeCellSioDev",  1, VXB_BUSID_PLB, 0, primeCellSioDev1Num , primeCellSioDev1Resources },
   { "primeCellSioDev",  2, VXB_BUSID_PLB, 0, primeCellSioDev2Num , primeCellSioDev2Resources },
   { "primeCellSioDev",  3, VXB_BUSID_PLB, 0, primeCellSioDev3Num , primeCellSioDev3Resources },
 
#ifdef DRV_VXBEND_FTGMAC
    { "gmac", 0, VXB_BUSID_PLB, 0, gmac0Num, gmac0Resources},
    { "gmac", 1, VXB_BUSID_PLB, 0, gmac1Num, gmac1Resources},
#endif

#ifdef  DRV_ARM_GEN_SYS_TIMER
    { "armGenTimer", 0, VXB_BUSID_PLB, 0, armGenSysTimerNum, armGenSysTimerResources },
#endif  /* DRV_ARM_GEN_SYS_TIMER */

#ifdef  DRV_ARM_GEN_AUX_TIMER
    { "armAuxTimer", 0, VXB_BUSID_PLB, 0, armAuxGenTimerNum, armAuxGenTimerResources },
#endif  /* DRV_ARM_GEN_SYS_TIMER */


#ifdef DRV_PCIBUS_FT
    { "ftPcie", 0, VXB_BUSID_PLB, 0, ftPci0Num, ftPci0Resources },
#endif /* DRV_PCIBUS_M85XX */

#ifdef DRV_FTCAN
{ "ftCan", 0, VXB_BUSID_PLB, 0, ftCanDev0Num, ftCanDev0Resources },
{ "ftCan", 1, VXB_BUSID_PLB, 0, ftCanDev1Num, ftCanDev1Resources },
/*{ "ftCan", 2, VXB_BUSID_PLB, 0, ftCanDev2Num, ftCanDev2Resources },*/
#endif

#ifdef INCLUDE_FT_SD
{ "ftSdhci",0,VXB_BUSID_PLB, 0,ftSdhc0Num, ftSdhc0Resources },
#endif

#ifdef INCLUDE_PC_CONSOLE
{ "m6845Vga", 0, VXB_BUSID_PLB, 0, pentiumM6845VgaNum, pentiumM6845VgaResources },
#endif

#ifdef DRV_FTI2C
{ "ftI2c", 0, VXB_BUSID_PLB, 0, i2cDev0Num, i2cDev0Resources },
{ "ftI2c", 1, VXB_BUSID_PLB, 0, i2cDev1Num, i2cDev1Resources },
/* CAN1,CAN2 & I2C_2: ONLY choose ONE. (pad pin demux) */
/*
{ "ftI2c", 2, VXB_BUSID_PLB, 0, i2cDev2Num, i2cDev2Resources },
{ "ftI2c", 3, VXB_BUSID_PLB, 0, i2cDev3Num, i2cDev3Resources },
*/
#endif
#ifdef DRV_FTQSPI
{ FT_QSPI_NAME,  0, VXB_BUSID_PLB, 0, qspiNum,  qspiResources },
#endif

#ifdef DRV_FTGPIO
{ "ftGpio", 0, VXB_BUSID_PLB, 0, gpioDev0Num, gpioDev0Resources },
{ "ftGpio", 1, VXB_BUSID_PLB, 0, gpioDev1Num, gpioDev1Resources },
#endif

#ifdef DRV_FTSCPI
{ "ftScpi", 0, VXB_BUSID_PLB, 0, ftScpiDev0Num, ftScpiDev0Resources },
#endif
};

const int hcfDeviceNum = NELEMENTS(hcfDeviceList);

VXB_INST_PARAM_OVERRIDE sysInstParamTable[] =
    {
    { NULL, 0, NULL, VXB_PARAM_END_OF_LIST, {(void *)0} }
    };


