/* vxbYt8521Phy.h - register definitions for yt8521 PHY chips */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCyt8521Phyh
#define __INCyt8521Phyh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void ytPhyRegister (void);

#define REG_DEBUG_ADDR_OFFSET             0x1e
#define REG_DEBUG_DATA                    0x1f

#define YT8521_EXTREG_SLEEP_CONTROL1      0x27
#define YT8521_EXTREG_SMI_SDS_PHY         0xA000
#define YT8521_EXTREG_C                   0xc

#define YT8521_EN_SLEEP_SW_BIT            15

#ifdef __cplusplus
}
#endif

#endif /* __INCyt8521Phyh */
