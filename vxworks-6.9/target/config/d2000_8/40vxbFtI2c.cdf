/* 40vxbFtI2c.cdf - I2C controller configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


Component   DRV_FTI2C {
   NAME        FT I2C controller driver
    SYNOPSIS    FT I2C controller driver
    _CHILDREN   FOLDER_DRIVERS
    CONFIGLETTES 
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    vxbFtI2cRegister();
    PROTOTYPE   void vxbFtI2cRegister (void);
    REQUIRES    INCLUDE_VXBUS \
                INCLUDE_PLB_BUS \
                INCLUDE_I2C_BUS
   INIT_AFTER  INCLUDE_PLB_BUS  \
                INCLUDE_I2C_BUS 
}

