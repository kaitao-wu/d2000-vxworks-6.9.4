/* vxbFtQspi.h - QSPI driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#ifndef __INCvxbFtQspih
#define __INCvxbFtQspih

#include <vxWorks.h>
#include <config.h>
#include <sysLib.h>
#include <vxBusLib.h>
#include <string.h>
#include <memLib.h>
#include <stdlib.h>
#include <semLibCommon.h>
#include <hwif/vxbus/vxbSpiLib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* defines */
#define FT_QSPI_NAME            "ftQspi"

/* chip selects supported */
#define QSPI_MAX_CS_NUM         (4)

/* qspi flash controller registers */
#define REG_BASE_QSPI           0x28014000
/* register definition */
#define REG_QSPI_CAP                (0x00)
#define REG_QSPI_RD_CFG             (0x04)
#define REG_QSPI_WR_CFG             (0x08)
#define REG_QSPI_FLUSH              (0x0C)
#define REG_QSPI_CMD_PORT           (0x10)
#define REG_QSPI_ADDR_PORT          (0x14)
#define REG_QSPI_HD_PORT            (0x18)
#define REG_QSPI_LD_PORT            (0x1C)
#define REG_QSPI_FUN_SET            (0x20)
#define REG_QSPI_WIP_RD             (0x24)
#define REG_QSPI_WP                 (0x28)
#define REG_QSPI_MODE               (0x2C)

/*QSPI_CAP*/
#define CAP_FLASH_NUM(data)         ((data) << 3)   
#define CAP_FLASH_CAP(data)         ((data) << 0)

/*QSPI_RD_CFG*/
#define RD_CFG_CMD(data)            ((data) << 24)  
#define RD_CFG_THROUGH(data)        ((data) << 23) 
#define RD_CFG_TRANSFER(data)       ((data) << 20) 
#define RD_CFG_ADDR_SEL(data)       ((data) << 19) 
#define RD_CFG_LATENCY(data)        ((data) << 18) 
#define RD_CFG_MODE_BYTE(data)      ((data) << 17) 
#define RD_CFG_CMD_SIGN(data)       ((data) << 9) 
#define RD_CFG_DUMMY(data)          ((data) << 4)
#define RD_CFG_D_BUFFER(data)       ((data) << 3) 
#define RD_CFG_SCK_SEL(data)        ((data) << 0)

/*QSPI_WR_CFG*/
#define WR_CFG_CMD(data)            ((data) << 24) 
#define WR_CFG_WAIT(data)           ((data) << 9) 
#define WR_CFG_THROUGH(data)        ((data) << 8) 
#define WR_CFG_TRANSFER(data)       ((data) << 5) 
#define WR_CFG_ADDRSEL(data)        ((data) << 4) 
#define WR_CFG_MODE(data)           ((data) << 3) 
#define WR_CFG_SCK_SEL(data)        ((data) << 0) 

/*QSPI_CMD_PORT*/
#define CMD_PORT_CMD(data)          ((data) << 24) 
#define CMD_PORT_WAIT(data)         ((data) << 22) 
#define CMD_PORT_THROUGH(data)      ((data) << 21) 
#define CMD_PORT_CS(data)           ((data) << 19) 
#define CMD_PORT_TRANSFER(data)     ((data) << 16) 
#define CMD_PORT_ADDR(data)         ((data) << 15) 
#define CMD_PORT_LATENCY(data)      ((data) << 14) 
#define CMD_PORT_DATA_TRANS(data)   ((data) << 13) 
#define CMD_PORT_ADDR_SEL(data)     ((data) << 12) 
#define CMD_PORT_DUMMY(data)        ((data) << 7) 
#define CMD_PORT_P_BUFFER(data)     ((data) << 6) 
#define CMD_PORT_RW_NUM(data)       ((data) << 3)
#define CMD_PORT_CLK_SEL(data)      ((data) << 0)   

/*QSPI_FUN_SET*/
#define FUN_SET_CS_HOLD(data)       ((data) << 24) 
#define FUN_SET_CS_SETUP(data)      ((data) << 16)
#define FUN_SET_CS_DELAY(data)      ((data) << 0)

/*QSPI_WIP_RD*/
#define WIP_RD_CMD(data)            ((data) << 24) 
#define WIP_RD_TRANSFER(data)       ((data) << 3)
#define WIP_RD_SCK_SEL(data)        ((data) << 0)
                                    
/*QSPI_WP*/
#define WP_EN(data)                 ((data) << 17) 
#define WP_WP(data)                 ((data) << 16) 
#define WP_HOLD(data)               ((data) << 8) 
#define WP_SETUP(data)              ((data) << 0) 

/*QSPI_MODE*/
#define MODE_VALID(data)            ((data) << 8) 
#define MODE_MODE(data)             ((data) << 0) 

#define QSPI_FLASH_CAP_4MB      0
#define QSPI_FLASH_CAP_8MB      1
#define QSPI_FLASH_CAP_16MB     2
#define QSPI_FLASH_CAP_32MB     3
#define QSPI_FLASH_CAP_64MB     4
#define QSPI_FLASH_CAP_128MB    5
#define QSPI_FLASH_CAP_256MB    6

#define QSPI_ADDR_SEL_3         0
#define QSPI_ADDR_SEL_4         1

#define QSPI_SCK_DIV_128        0
#define QSPI_SCK_DIV_2          1
#define QSPI_SCK_DIV_4          2
#define QSPI_SCK_DIV_8          3
#define QSPI_SCK_DIV_16         4
#define QSPI_SCK_DIV_32         5
#define QSPI_SCK_DIV_64         6 

#define QSPI_TRANSFER_1_1_1     0
#define QSPI_TRANSFER_1_1_2     1
#define QSPI_TRANSFER_1_1_4     2
#define QSPI_TRANSFER_1_2_2     3
#define QSPI_TRANSFER_1_4_4     4
#define QSPI_TRANSFER_2_2_2     5
#define QSPI_TRANSFER_4_4_4     6

/* qspi command instruction */
#define QSPI_FLASH_CMD_WRR         0x01  /* Write status register */
#define QSPI_FLASH_CMD_PP          0x02  /* Page program */
#define QSPI_FLASH_CMD_READ        0x03  /* Normal read data bytes */
#define QSPI_FLASH_CMD_WRDI        0x04  /* Write disable */
#define QSPI_FLASH_CMD_RDSR1       0x05  /* Read status register */
#define QSPI_FLASH_CMD_WREN        0x06  /* Write enable */
#define QSPI_FLASH_CMD_RDSR2       0x07  /* Read status register */
#define QSPI_FLASH_CMD_FAST_READ   0x0B  /* Fast read data bytes */
#define QSPI_FLASH_CMD_4FAST_READ  0x0C  /* Fast read data bytes */
#define QSPI_FLASH_CMD_4PP         0x12  /* Page program */
#define QSPI_FLASH_CMD_4READ       0x13  /* Normal read data bytes */
#define QSPI_FLASH_CMD_P4E         0x20  /* Erase 4kb sector */
#define QSPI_FLASH_CMD_4P4E        0x21  /* Erase 4kb sector */
#define QSPI_FLASH_CMD_QPP         0x32  /* Quad Page program */
#define QSPI_FLASH_CMD_4QPP        0x34  /* Quad Page program */
#define QSPI_FLASH_CMD_RDCR        0x35  /* Read config register */
#define QSPI_FLASH_CMD_BE          0x60  /* Bulk erase */
#define QSPI_FLASH_CMD_RDAR        0x65  /* Read Any Register  */
#define QSPI_FLASH_CMD_QOR         0x6B  /* Quad read data bytes */
#define QSPI_FLASH_CMD_4QOR        0x6C  /* Quad read data bytes */
#define QSPI_FLASH_CMD_WRAR        0x71  /* Write Any Register  */
#define QSPI_FLASH_CMD_RDID        0x9F  /* Read JEDEC ID */
#define QSPI_FLASH_CMD_4BAM        0xB7  /* Enter 4 Bytes Mode */
#define QSPI_FLASH_CMD_4BE         0xC7  /* Bulk erase */
#define QSPI_FLASH_CMD_SE          0xD8  /* Sector erase */
#define QSPI_FLASH_CMD_4SE         0xDC  /* Sector erase */
#define QSPI_FLASH_CMD_4BEX        0xE9  /* Exit 4 Bytes Mode */    
#define QSPI_FLASH_CMD_QIOR        0xEB  /* Quad read data bytes */
#define QSPI_FLASH_CMD_4QIOR       0xEC  /* Quad read data bytes */

/* S25FL256 status register bit defined  */
#define SR_SRWD          (0x1 << 7)         /* SR write protect */
#define SR_PERR          (0x1 << 6)         /* Program err bit */
#define SR_EERR          (0x1 << 5)         /* erase err bit */
#define SR_BP2           (0x1 << 4)         /* Block protect 2 */
#define SR_BP1           (0x1 << 3)         /* Block protect 1 */
#define SR_BP0           (0x1 << 2)         /* Block protect 0 */
#define SR_WEL           (0x1 << 1)         /* Write enable latch */
#define SR_WIP           (0x1 << 0)         /* Write in progress */                                    

#define WIP_TOUT         300000             /* max 300s */
                                    
/* typedefs */
/*QSPI_RD_CFG*/
typedef union
    {
    UINT32 data;
    struct
        {
        UINT32 rd_sck_sel :3;       /* 2:0   */
        UINT32 d_buffer :1;         /* 3     */
        UINT32 dummy :5;            /* 8:4   */     
        UINT32 cmd_sign :8;         /* 16:9  */     
        UINT32 mode_byte :1;        /* 17    */     
        UINT32 rd_latency :1;       /* 18    */     
        UINT32 rd_addr_sel :1;      /* 19    */  
        UINT32 rd_transfer :3;      /* 22:20 */       
        UINT32 rd_through :1;       /* 23    */       
        UINT32 rd_cmd :8;           /* 31:24 */                
        } val;
    } QSPI_RD_CFG_REG_T;

/*QSPI_WR_CFG*/
typedef union
    {
    UINT32 data;
    struct
        {
        UINT32 wr_sck_sel :3;       /* 2:0   */     
        UINT32 wr_mode :1;          /* 3     */     
        UINT32 wr_addrsel :1;       /* 4     */     
        UINT32 wr_transfer :3;      /* 7:5   */     
        UINT32 wr_through :1;       /* 8     */     
        UINT32 wr_wait :1;          /* 9     */     
        UINT32 wr_res :14;          /* 23:10 */             
        UINT32 wr_cmd :8;           /* 31:24 */
        } val;
    } QSPI_WR_CFG_REG_T;
    
/*QSPI_CMD_PORT*/
typedef union
    {
    UINT32 data;
    struct
        {
        UINT32 sck_sel :3;      /* 2:0   */         
        UINT32 rw_mum :3;       /* 5:3   */     
        UINT32 p_buffer :1;     /* 6     */     
        UINT32 dummy :5;        /* 11:7  */     
        UINT32 addr_sel :1;     /* 12    */     
        UINT32 data_transfer :1;/* 13    */     
        UINT32 latency :1;      /* 14    */     
        UINT32 cmd_addr :1;     /* 15    */     
        UINT32 transfer :3;     /* 18:16 */     
        UINT32 cs :2;           /* 20:19 */     
        UINT32 through :1;      /* 21    */     
        UINT32 wait :1;         /* 22    */     
        UINT32 res :1;          /* 23    */             
        UINT32 cmd :8;          /* 31:24 */
        } val;
    } QSPI_CMD_PORT_REG_T;    
        

/* structure holding the instance specific details */
typedef struct tf_qspi_drv_ctrl
    {
    VXB_DEVICE_ID    pDev;
    void *           regBase;
    void *           regHandle;
    UINT32           capacity;
    UINT32           clkDiv;
    UINT32           transMode;
    UINT32           addrMode;
    UINT32           spiDevNum;
    UINT32           channel;
    UINT32           initPhase;
    UINT8 *          txBuf;
    UINT32           txLen;
    UINT8 *          rxBuf;
    UINT32           rxLen;
    BOOL             initDone;
    SEM_ID           muxSem;
    VXB_SPI_BUS_CTRL vxbSpiCtrl;
    } FT_QSPI_CTRL; 

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFtQspih */
