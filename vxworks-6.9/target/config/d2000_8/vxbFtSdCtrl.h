/* vxbFtSdCtrl.h - SD card driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbFtSdCtrlh
#define __INCvxbFtSdCtrlh

#ifdef __cplusplus
extern "C" {
#endif


#define FT_SDHC_NAME "ftSdhci"

#define SDHC_MAX_RW_SECTORS         512
/* SDCI CMD_SETTING_REG */
#define SDCI_CMD_ADTC_MASK          (0x00008000)    /* ADTC*/
#define SDCI_CMD_CICE_MASK          (0x00000010)    /**/
#define SDCI_CMD_CRCE_MASK          (0x00000008)    /**/
#define SDCI_CMD_RES_NONE_MASK      (0x00000000)    /* no response */
#define SDCI_CMD_RES_LONG_MASK      (0x00000001)    /* long response */
#define SDCI_CMD_RES_SHORT_MASK     (0x00000002)    /* short response */



/* SDCI_NORMAL_ISR mask */
#define SDCI_NORMAL_ISR_CC               (0x1 << 0)     /* R */
#define SDCI_NORMAL_ISR_CR               (0x1 << 1)     /* R */
#define SDCI_NORMAL_ISR_CI               (0x1 << 2)     /* R */
#define SDCI_NORMAL_ISR_TIMEOUT          (0x1 << 3)     /* R */


#define SDCI_SD_DRV_VALUE           0
#define SDCI_SD_SAMP_VALUE_MAX      50
#define SDCI_SD_SAMP_VALUE_MIN      0

#define SDCI_TIMEOUT_DATA_VALUE 2000000


#define SDCI_TIMEOUT_CMD_VALUE  0xFFFFFFFF
#define SDCI_POWER_ON           1
#define SDCI_POWER_OFF          0

#define SDCI_CMD_TIMEOUT        10
#define SDCI_DAT_TIMEOUT        5000
#define SDCI_CMD_TYPE_ADTC      0x2

#define SDCI_F_MIN              400000
#define SDCI_F_MAX              30000000

/* SDCI_NORMAL_ISER mask */
#define SDCI_SDCI_NORMAL_ISER_ECC_EN     (0x1 << 0)     /* RW */
#define SDCI_SDCI_NORMAL_ISER_ECR        (0x1 << 1)     /* RW */
#define SDCI_SDCI_NORMAL_ISER_EEI_EN     (0x1 << 15)    /* RW */



/* SDCI_SOFTWARE mask */
#define SDCI_SOFTWARE_SRST               (0x1 << 0)     /* RW */
#define SDCI_SOFTWARE_BDRST              (0x1 << 2)     /* RW */
#define SDCI_SOFTWARE_CFCLF              (0x1 << 3)     /* RW */


/* SDCI_STATUS mask */
#define SDCI_STATUS_CMD_BUSY             (0x0 << 0)     /* R */
#define SDCI_STATUS_CMD_READY            (0x1 << 0)     /* R */
#define SDCI_STATUS_IDIE                 (0x1 << 12)    /* R */


/* SDCI NORMAL_INT_STATUS_REG */
#define SDCI_NORMAL_EI_STATUS   (0x00008000)    /* command error */
#define SDCI_NORMAL_CR_STATUS   (0x00000002)    /* card remove */
#define SDCI_NORMAL_CC_STATUS   (0x00000001)    /* command done */

#define MMC_CMD_ALL_SEND_CID        2



#define SDCI_CONTROLLER                ( 0x00)    /* the controller config reg               */
#define SDCI_ARGUMENT                   (0x04)    /* the argument reg                        */
#define SDCI_COMMAND                    (0x08)    /* the command reg                         */
#define SDCI_CLOCK_D                      (0x0c)    /* the clock divide reg                    */
#define SDCI_SOFTWARE                  (0x10)    /* the controller reset reg                */
#define SDCI_POWER                        (0x14)   /* THE POWRE CONTROL REG                   */
#define SDCI_TIMEOUT_CMD            (0x18) /* the cmd timeout config reg              */
#define SDCI_TIMEOUT_DATA          (0x1c)

#define SDCI_NORMAL_ISER             (0x20)   /* the normal ISR config reg               */
#define SDCI_ERROR_ISER                (0x24)   /* the erroe ISR config reg                */
#define SDCI_BD_ISER                      (0x28)   /* the BD ISR config reg                   */
#define SDCI_SD_DRV                       (0x30)    /* the SD card driving phase position reg  */
#define SDCI_SD_SAMP                    (0x34)    /* the SD card sampling phase position reg */
#define SDCI_SD_SEN                      (0x38)        /* the SD card detection reg               */
#define SDCI_HDS_AXI                     (0x3c)        /* AIX boundary config reg                 */
#define SDCI_BD_RX                         (0x40)        /* the BD rx addr reg                      */
#define SDCI_BD_TX                        ( 0x60)       /* the BD tx addr reg                      */
#define SDCI_BLK_CNT                     (0x80)        /* the r/w block num reg                   */
#define SDCI_NORMAL_ISR             ( 0xC0)       /* the normal ISR status reg               */

#define SDCI_ERROR_ISR                (0xC4)   /* the error ISR status reg                */
#define SDCI_BD_ISR                     ( 0xC8)    /* the BD ISR status reg                   */

#define SDCI_STATUS                     (0xD0)    /* the status reg                          */
#define SDCI_BLOCK                       ( 0xD4)     /* the block len reg                       */
#define SDCI_RESP0                       ( 0xE0)     /* response reg0                           */
#define SDCI_RESP1                      ( 0xE4)     /* response reg1                           */
#define SDCI_RESP2                     ( 0xE8)     /* response reg2                           */
#define SDCI_RESP3                     ( 0xEC)     /* response reg3                           */

#define SD_BLOCK_SIZE       512 
#define MMC_DATA_WRITE      BIT(8)
#define MMC_DATA_READ       BIT(9)
/* Extra flags used by CQE */
#define MMC_DATA_QBR        BIT(10)     /* CQE queue barrier*/
#define MMC_DATA_PRIO       BIT(11)     /* CQE high priority */
#define MMC_DATA_REL_WR     BIT(12)     /* Reliable write */
#define MMC_DATA_DAT_TAG    BIT(13)     /* Tag request */
#define MMC_DATA_FORCED_PRG BIT(14)     /* Forced programming */


#define MMC_GO_IDLE_STATE         0   /* bc                          */
#define MMC_SEND_OP_COND          1   /* bcr  [31:0] OCR         R3  */
#define MMC_ALL_SEND_CID          2   /* bcr                     R2  */
#define MMC_SET_RELATIVE_ADDR     3   /* ac   [31:16] RCA        R1  */
#define MMC_SET_DSR               4   /* bc   [31:16] RCA            */
#define MMC_SLEEP_AWAKE       5   /* ac   [31:16] RCA 15:flg R1b */
#define MMC_SWITCH                6   /* ac   [31:0] See below   R1b */
#define MMC_SELECT_CARD           7   /* ac   [31:16] RCA        R1  */
#define MMC_SEND_EXT_CSD          8   /* adtc                    R1  */
#define MMC_SEND_CSD              9   /* ac   [31:16] RCA        R2  */
#define MMC_SEND_CID             10   /* ac   [31:16] RCA        R2  */
#define MMC_READ_DAT_UNTIL_STOP  11   /* adtc [31:0] dadr        R1  */
#define MMC_STOP_TRANSMISSION    12   /* ac                      R1b */
#define MMC_SEND_STATUS          13   /* ac   [31:16] RCA        R1  */
#define MMC_BUS_TEST_R           14   /* adtc                    R1  */
#define MMC_GO_INACTIVE_STATE    15   /* ac   [31:16] RCA            */
#define MMC_BUS_TEST_W           19   /* adtc                    R1  */
#define MMC_SPI_READ_OCR         58   /* spi                  spi_R3 */
#define MMC_SPI_CRC_ON_OFF       59   /* spi  [0:0] flag      spi_R1 */


/* SDCI ERROR_INT_STATUS_REG */
#define SDCI_ERROR_CNR_ERROR_STATUS     (0x00000010)    /**/
#define SDCI_ERROR_CIR_ERROR_STATUS     (0x00000008)    /**/
#define SDCI_ERROR_CCRCE_ERROR_STATUS   (0x00000002)    /**/
#define SDCI_ERROR_CTE_ERROR_STATUS     (0x00000001)    /**/


/* SDCI BD_ISR_REG */
#define SDCI_BD_DAIS_ERROR_STATUS       (0x00008000)    /* */
#define SDCI_BD_RESPE_STATUS            (0x00000100)    /* */
#define SDCI_BD_DATFRAX_ERROR_STATUS    (0x00000080)    /* */
#define SDCI_BD_NRCRC_ERROR_STATUS      (0x00000040)    /* */
#define SDCI_BD_TRE_ERROR_STATUS        (0x00000020)    /* */
#define SDCI_BD_CMDE_ERROR_STATUS       (0x00000010)    /* */
#define SDCI_BD_DTE_ERROR_STATUS        (0x00000008)    /* */
#define SDCI_BD_TRS_STATUS              (0x00000001)    /* */


#ifdef SDCI_BDINT_MODE_0
#define SDCI_BD_ISR_TRS                  (0x1 << 0)     /* R */
#else
#define SDCI_BD_ISR_TRS_W                (0x1 << 0)     /* R */  /*the hardware update*/
#define SDCI_BD_ISR_TRS_R                (0x1 << 8)     /* R */  /*the hardware update*/
#endif
#define SDCI_BD_ISR_EDTE                 (0x1 << 3)     /* R */


/* SDCI_HDS_AXI mask */
#define SDCI_HDS_AXI_AWDOMAIN            (0x1 << 0)     /* RW */
#define SDCI_HDS_AXI_ARDOMAIN            (0x1 << 12)    /* RW */
#define SDCI_HDS_AXI_AWCACHE             (0x6 << 24)    /* RW */
#define SDCI_HDS_AXI_ARCACHE             (0xB << 28)    /* RW */

/* SDCI_STATUS mask */
#define SDCI_STATUS_CMD_BUSY             (0x0 << 0)     /* R */
#define SDCI_STATUS_CMD_READY            (0x1 << 0)     /* R */
#define SDCI_STATUS_IDIE                 (0x1 << 12)    /* R */


/* SDCI_NORMAL_ISR mask */
#define SDCI_NORMAL_ISR_CC               (0x1 << 0)     /* R */
#define SDCI_NORMAL_ISR_CR               (0x1 << 1)     /* R */

#define SDCI_NORMAL_ISR_TIMEOUT          (0x1 << 3)     /* R */


/* SDCI_ERROR_ISER mask */
#define SDCI_ERROR_ISER_ECTE_EN              (0x1 << 0)     /* RW */

/* SDCI_ERROR_ISR mask */
#define SDCI_ERROR_ISR_CTE               (0x1 << 0)     /* R */

/* SDCI_BD_ISER mask */
#ifdef SDCI_BDINT_MODE_0
#define SDCI_BD_ISER_ETRS_EN             (0x1 << 0)     /* RW */
#else
#define SDCI_BD_ISER_ETRS_EN             (0x1 << 8)     /* RW */  /*the hardware update*/
#endif


#define MMC_APP_CMD              55 
#define SD_APP_SET_BUS_WIDTH      6

#define SDCI_CMD_INDEX_MASK     (0x00003F00)
#define SDCI_CMD_INDEX_SHIFT    (8)


#define MMC_RSP_PRESENT (1 << 0)
#define MMC_RSP_136 (1 << 1)        /* 136 bit response */
#define MMC_RSP_CRC (1 << 2)        /* expect valid crc */
#define MMC_RSP_BUSY    (1 << 3)        /* card may send busy */
#define MMC_RSP_OPCODE  (1 << 4)        /* response contains opcode */

#define SDCI_SDCI_NORMAL_ISER_ECC_EN     (0x1 << 0)
#define SDCI_BD_ISER_ETRS_EN             (0x1 << 8)

#define SDCI_SOURCE_CLOCK_RATE  48000000

/* SDCI SOFTWARE_RESET_REG */
#define SDCI_RESET_CFCLF    (0x00000008)    /* 0 */
#define SDCI_BDRST          (0x00000004)    /* DMA BD set */
#define SDCI_SRST           (0x00000001)    /* soft reset controller */

/*irq */
#define IRQ_err  0x1<<15
#define IRQ_CMD_FT  0X1<<0
#define IRQ_DMA
#define IRQ_REMOVE 0X1<<1

#define MMC_CMD23_ARG_REL_WR    (1 << 31)
#define MMC_CMD23_ARG_PACKED    ((0 << 31) | (1 << 30))
#define MMC_CMD23_ARG_TAG_REQ   (1 << 29)

unsigned int        flags;      /* expected response type */


#define MMC_CMD_MASK    (3 << 5)        /* non-SPI command type */
#define MMC_CMD_AC  (0 << 5)
#define MMC_CMD_ADTC    (1 << 5)
#define MMC_CMD_BC  (2 << 5)
#define MMC_CMD_BCR (3 << 5)

#define MMC_RSP_SPI_S1  (1 << 7)        /* one status byte */
#define MMC_RSP_SPI_S2  (1 << 8)        /* second byte */
#define MMC_RSP_SPI_B4  (1 << 9)        /* four data bytes */
#define MMC_RSP_SPI_BUSY (1 << 10)      /* card may send busy */

#define SDCI_SDCI_NORMAL_ISER_ECI        (0x1 << 2)

/* SDHC driver control */

typedef struct ftsdhcDrvCtrl
    {
    SD_HOST_CTRL    sdHostCtrl;
    VXB_DEVICE_ID           pDev;   
    void *          regBase;
    void *          regHandle;
    struct vxbSdioInt * pIntInfo;
    UINT32          intSts;
    UINT32          intMask;
    } FTSDHC_DEV_CTRL;
/* register low level access routines */

#define FTSDHC_BAR(p)         ((FTSDHC_DEV_CTRL *)(p)->pDrvCtrl)->regBase
#define FTSDHC_HANDLE(p)      ((FTSDHC_DEV_CTRL *)(p)->pDrvCtrl)->regHandle

#define CSR_READ_4(pDev, addr)              \
        vxbRead32(FTSDHC_HANDLE(pDev),       \
                  (UINT32 *)((char *)FTSDHC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)       \
        vxbWrite32(FTSDHC_HANDLE(pDev),      \
                   (UINT32 *)((char *)FTSDHC_BAR(pDev) + addr), data)
#define CSR_SETBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)          \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & (UINT32)(~(val)))
               
    void sdhcCtrlInstInit(VXB_DEVICE_ID);
    void sdhcCtrlInstInit2 (VXB_DEVICE_ID);
    STATUS sdhcCtrlInstConnect (SD_HOST_CTRL * pSdHostCtrl);
    STATUS sdhcCtrlIsr (VXB_DEVICE_ID);
    STATUS sdhcCtrlCmdIssue (VXB_DEVICE_ID, SD_CMD *);
    STATUS sdhcCtrlCmdPrepare (VXB_DEVICE_ID, SD_CMD *);
    STATUS sdhcCtrlCmdIssuePoll (VXB_DEVICE_ID, SD_CMD *);
    STATUS sdhcCtrlInit (VXB_DEVICE_ID);
    STATUS sdhcInterruptInfo (VXB_DEVICE_ID, UINT32 *);
    STATUS sdhcDevControl (VXB_DEVICE_ID, pVXB_DEVCTL_HDR);
    
    void sdhcCtrlBusWidthSetup (VXB_DEVICE_ID, UINT32);
    void sdhcCtrlCardMonTaskPoll (VXB_DEVICE_ID);
    void ftsdhcCtrlClkFreqSetup (VXB_DEVICE_ID, UINT32);
    BOOL sdhcCtrlCardWpCheck (VXB_DEVICE_ID);
    BOOL sdhcCtrlCardInsertSts (VXB_DEVICE_ID);

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFtSdCtrlh */

