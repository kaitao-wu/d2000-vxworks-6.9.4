/* vxbFtGmac.h - GMAC driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCvxbFtGmacEndh
#define __INCvxbFtGmacEndh

#ifdef __cplusplus
extern "C" {
#endif

IMPORT void ftGmacRegister (void);

#define GMAC_BUF_BOUND	        _CACHE_ALIGN_SIZE
#define GMAC_ALIGN(x)   \
        (char *)(((UINT32)(x) + (GMAC_BUF_BOUND-1)) & ~(GMAC_BUF_BOUND-1))   
#define GMAC_BUF_ADJ(x)	        (x)->m_data = GMAC_ALIGN((x)->m_data)
#define MAC_MAX_FRAME_SZ        (2048)

#define GMAC_INC_DESC(x, y)     (x) = (((x) + 1) % y)
#define GMAC_MAXFRAG            32
#define GMAC_MAX_RX             32

#define GMAC_RX_DESC_CNT        4096
#define GMAC_TX_DESC_CNT        4096

#define GMAC_MTU                1500
#define GMAC_JUMBO_MTU          9000
#define GMAC_NAME               "gmac"
#define GMAC_TIMEOUT            100000

#define MII_BUSY 0x00000001

#undef MII_WRITE
#define MII_WRITE 0x00000002

#define BITS_PER_LONG 32
#define BITS_PER_WORD 32

#define BIT(bitnum)             (1 << (bitnum % BITS_PER_WORD))

#define GENMASK(h, l) \
    (((~0UL) - (1UL << (l)) + 1) & (~0UL >> (BITS_PER_LONG - 1 - (h))))

#define GMAC_CONTROL        0x00000000  /* Configuration */
#define GMAC_FRAME_FILTER   0x00000004  /* Frame Filter */
#define GMAC_HASH_HIGH      0x00000008  /* Multicast Hash Table High */
#define GMAC_HASH_LOW       0x0000000c  /* Multicast Hash Table Low */
#define GMAC_MII_ADDR       0x00000010  /* MII Address */
#define GMAC_MII_DATA       0x00000014  /* MII Data */
#define GMAC_FLOW_CTRL      0x00000018  /* Flow Control */
#define GMAC_VLAN_TAG       0x0000001c  /* VLAN Tag */
#define GMAC_VERSION        0x00000020  /* GMAC CORE Version */

#define GMAC_INT_STATUS     0x00000038  /* interrupt status register */
#define GMAC_INT_STATUS_LINKCHANGED BIT(0)
#define GMAC_INT_STATUS_PMT     BIT(3)
#define GMAC_INT_STATUS_MMCIS   BIT(4)
#define GMAC_INT_STATUS_MMCRIS  BIT(5)
#define GMAC_INT_STATUS_MMCTIS  BIT(6)
#define GMAC_INT_STATUS_MMCCSUM BIT(7)
#define GMAC_INT_STATUS_TSTAMP  BIT(9)
#define GMAC_INT_STATUS_LPIIS   BIT(10)

/* interrupt mask register */
#define GMAC_INT_MASK       0x0000003c
#define GMAC_INT_DISABLE_RGMII      BIT(0)
#define GMAC_INT_DISABLE_PCSLINK    BIT(1)
#define GMAC_INT_DISABLE_PCSAN      BIT(2)
#define GMAC_INT_DISABLE_PMT        BIT(3)
#define GMAC_INT_DISABLE_TIMESTAMP  BIT(9)
#define GMAC_INT_DISABLE_PCS    (GMAC_INT_DISABLE_RGMII | \
                 GMAC_INT_DISABLE_PCSLINK | \
                 GMAC_INT_DISABLE_PCSAN)
#define GMAC_INT_DEFAULT_MASK   (GMAC_INT_DISABLE_TIMESTAMP | \
                 GMAC_INT_DISABLE_PCS)

#define GMAC_HI_REG_AE      0x80000000

/* GMAC HW ADDR regs */
#define GMAC_ADDR0_HIGH     0x00000040  /* MAC Address 0 High */
#define GMAC_ADDR0_LOW      0x00000044  /* MAC Address 0 Low */
#define GMAC_ADDR1_HIGH     0x00000048  /* MAC Address 1 High */
#define GMAC_ADDR1_LOW      0x0000004c  /* MAC Address 1 Low */
#define GMAC_GMII_STATUS    0x000000d8  /* S/R-GMII status */

/* MMC control */
#define MMC_CNTRL             0x0100    /* MMC Control */
#define MMC_RX_INTR           0x0104    /* MMC RX Interrupt */
#define MMC_TX_INTR           0x0108    /* MMC TX Interrupt */
#define MMC_RX_INTR_MASK      0x010c    /* MMC Interrupt Mask */
#define MMC_TX_INTR_MASK      0x0110    /* MMC Interrupt Mask */
#define MMC_RX_IPC_INTR_MASK  0x0200
#define MMC_RX_IPC_INTR       0x0208
#define MMC_DEFAULT_MASK      0xffffffff

/* DMA CRS Control and Status Register Mapping */
#define DMA_BUS_MODE        0x00001000  /* Bus Mode */
#define DMA_XMT_POLL_DEMAND 0x00001004  /* Transmit Poll Demand */
#define DMA_RCV_POLL_DEMAND 0x00001008  /* Received Poll Demand */
#define DMA_RCV_BASE_ADDR   0x0000100c  /* Receive List Base */
#define DMA_TX_BASE_ADDR    0x00001010  /* Transmit List Base */
#define DMA_STATUS          0x00001014  /* Status Register */
#define DMA_OP_MODE         0x00001018  /* Ctrl (Operational Mode) */
#define DMA_INTR_ENA        0x0000101c  /* Interrupt Enable */
#define DMA_MISSED_FRAME_CTR    0x00001020  /* Missed Frame Counter */
#define DMA_RX_WATCHDOG     0x00001024    /* Receive Interrupt Watchdog */

#define DMA_BUS_RPBL(x) ((x & 0x3f) << 17)
#define DMA_BUS_FB      (1 << 16)     /* Fixed Burst */
#define DMA_BUS_PBL(x)  ((x & 0x3f) << 8)
#define DMA_BUS_DSL(x)  ((x & 0x1F) << 2)                                      /* Descriptor Skip Length */
#define DMA_BUS_DA      (1 << 1)      /* DMA Arbitration Scheme,Rx High Pro */
#define DMA_BUS_SWR     (1 << 0)      /* Software Reset */
#define DMA_BUS_INIT  (DMA_BUS_FB | DMA_BUS_PBL(16) | DMA_BUS_RPBL(16))

#define DMA_OP_DT     (1 << 26)       /* No Dropping of TCP/IP csum Err Frame */
#define DMA_OP_RSF    (1 << 25)       /* Rx Store and Forward */
#define DMA_OP_TSF    (1 << 21)       /* Tx Store and Forward */
#define DMA_OP_FTF    (1 << 20)       /* Flush Tx FIFO */
#define DMA_OP_TTC(x) ((x & 7) << 14) /* Tx Threshold Control */
#define DMA_OP_ST     (1 << 13)       /* Start/Stop Tx */
#define DMA_OP_RFD(x) ((x & 3) << 11) /* Threshold for DeActive Flow Control */
#define DMA_OP_RFA(x) ((x & 3) << 9)  /* Threshold for Active Flow Control */
#define DMA_OP_EFC    (1 << 8)        /* Enable HW Flow control */
#define DMA_OP_FEF    (1 << 7)        /* Forward Error Frame */
#define DMA_OP_FUF    (1 << 6)        /* Forward Undersize Good Frame */
#define DMA_OP_RTC(x) ((x & 3) << 3)  /* Rx Threshold Control */
#define DMA_OP_OSF    (1 << 2)        /* Operate On Second Mode */
#define DMA_OP_SR     (1 << 1)        /* Start/Stop Rx */

#define DMA_OP_INIT  (DMA_OP_TSF | DMA_OP_OSF | DMA_OP_RSF)

#define CFG_SMII_FTERR   (1 << 26)  /* SMII Force Tx Error */
#define CFG_CST          (1 << 25)  /* CRC stripping for type frames */
#define CFG_TX_CONFIG    (1 << 24)  /* Tx Configuration in RGMII/SGMII/SMII */
#define CFG_WD_DIS       (1 << 23)  /* Watchdog Disable */
#define CFG_JABBER_DIS   (1 << 22)  /* Jabber Disable */
#define CFG_FRAME_BST_EN (1 << 21)  /* Frame Burst Enable */
#define CFG_JUMBO_EN     (1 << 20)  /* Jumbo Frame Enable */
#define CFG_IFG(x)       (((x) & 7) << 17) /* Inter-Frame Gap */
#define CFG_DIS_CRS      (1 << 16)  /* Disable carrier sense during Tx */
#define CFG_MII_PORT_SEL (1 << 15)  /* Port Select 0:GMII, 1:MII */
#define CFG_FES_100      (1 << 14)  /* Seep 0:10,1:100 */
#define CFG_DIS_RX_OWN   (1 << 13)  /* Disable Rx Own */
#define CFG_LOOPBACK     (1 << 12)  /* Loop-back Mode */
#define CFG_DPLX_MODE    (1 << 11)  /* Duplex Mode */
#define CFG_IPC          (1 << 10)  /* Checksum offload */
#define CFG_DIS_RETRY    (1 << 9)   /* Disable Retry */
#define CFG_LINK_UD      (1 << 8)   /* LINK Up/Down */
#define CFG_AUTO_PCS     (1 << 7)   /* Automatic Pad/CRC Stripping */
#define CFG_DEF_CHECK    (1 << 4)   /* Deferral Check */
#define CFG_TX_EN        (1 << 3)   /* Tx Enable */
#define CFG_RX_EN        (1 << 2)   /* Rx Enable */

#define GMAC_INIT (CFG_DIS_RX_OWN | CFG_JABBER_DIS | CFG_AUTO_PCS | CFG_IPC | \
                       CFG_FRAME_BST_EN | CFG_DPLX_MODE | CFG_WD_DIS | CFG_CST)


/* DMA Status register */

#define DMA_LINE_INTF_INT       0x04000000  /* Line interface interrupt */
#define DMA_ERROR_BIT2          0x02000000  /* err. 0-data buffer, 1-desc. access, read only */
#define DMA_ERROR_BIT1          0x01000000  /* err. 0-write trnsf, 1-read transfr, read only */
#define DMA_ERROR_BIT0          0x00800000  /* err. 0-Rx DMA, 1-Tx DMA, read only */
#define DMA_TX_STATE            0x00700000  /* Transmit process state, read only */
#define DMA_TX_STOPPED          0x00000000  /* Stopped */
#define DMA_TX_FETCHING         0x00100000  /* Running - fetching the descriptor */
#define DMA_TX_WAITING          0x00200000  /* Running - waiting for end of transmission */
#define DMA_TX_READING          0x00300000  /* Running - reading the data from memory */
#define DMA_TX_SUSPENDED        0x00600000  /* Suspended */
#define DMA_TX_CLOSING          0x00700000  /* Running - closing descriptor */
#define DMA_RX_STATE            0x000E0000  /* Receive process state, read only */
#define DMA_RX_STOPPED          0x00000000  /* Stopped */
#define DMA_RX_FETCHING         0x00020000  /* Running - fetching the descriptor */
#define DMA_RX_WAITING          0x00060000  /* Running - waiting for packet */
#define DMA_RX_SUSPENDED        0x00080000  /* Suspended */
#define DMA_RX_CLOSING          0x000A0000  /* Running - closing descriptor */
#define DMA_RX_QUEUING          0x000E0000  /* Running - queuing the recieve frame into host memory */
#define DMA_INT_NORMAL          0x00010000  /* Normal interrupt summary, RW */
#define DMA_INT_ABNORMAL        0x00008000  /* Abnormal interrupt summary, RW */
#define DMA_INT_EARLY_RX        0x00004000  /* Early receive interrupt (Normal)       RW        0       */
#define DMA_INT_BUS_ERROR       0x00002000  /* Fatal bus error (Abnormal)             RW        0       */
#define DMA_INT_EARLY_TX        0x00000400  /* Early transmit interrupt (Abnormal)    RW        0       */
#define DMA_INT_RX_WDOG_TO      0x00000200  /* Receive Watchdog Timeout (Abnormal)    RW        0       */
#define DMA_INT_RX_STOPPED      0x00000100  /* Receive process stopped (Abnormal)     RW        0       */
#define DMA_INT_RX_NO_BUFFER    0x00000080  /* Receive buffer unavailable (Abnormal)  RW        0       */
#define DMA_INT_RX_COMPLETED    0x00000040  /* Completion of frame reception (Normal) RW        0       */
#define DMA_INT_TX_UNDERFLOW    0x00000020  /* Transmit underflow (Abnormal)          RW        0       */
#define DMA_INT_RX_OVERFLOW     0x00000010  /* Receive Buffer overflow interrupt      RW        0       */
#define DMA_INT_TX_JABBER_TO    0x00000008  /* Transmit Jabber Timeout (Abnormal)     RW        0       */
#define DMA_INT_TX_NO_BUFFER    0x00000004  /* Transmit buffer unavailable (Normal)   RW        0       */
#define DMA_INT_TX_STOPPED      0x00000002  /* Transmit process stopped (Abnormal)    RW        0       */
#define DMA_INT_TX_COMPLETED    0x00000001  /* Transmit completed (Normal)            RW        0       */


/* DMA default interrupt mask */
#define DMA_INT_ENABLE (DMA_INT_NORMAL | DMA_INT_ABNORMAL | DMA_INT_BUS_ERROR \
            | DMA_INT_RX_NO_BUFFER | DMA_INT_RX_COMPLETED | DMA_INT_RX_STOPPED\
            | DMA_INT_TX_UNDERFLOW | DMA_INT_TX_COMPLETED | DMA_INT_TX_STOPPED)

/* Normal receive descriptor defines */

/* RDES0 */
#define RDES0_PAYLOAD_CSUM_ERR  BIT(0)
#define RDES0_CRC_ERROR         BIT(1)
#define RDES0_DRIBBLING         BIT(2)
#define RDES0_MII_ERROR         BIT(3)
#define RDES0_RECEIVE_WATCHDOG  BIT(4)
#define RDES0_FRAME_TYPE        BIT(5)
#define RDES0_COLLISION         BIT(6)
#define RDES0_IPC_CSUM_ERROR    BIT(7)
#define RDES0_LAST_DESCRIPTOR   BIT(8)
#define RDES0_FIRST_DESCRIPTOR  BIT(9)
#define RDES0_VLAN_TAG          BIT(10)
#define RDES0_OVERFLOW_ERROR    BIT(11)
#define RDES0_LENGTH_ERROR      BIT(12)
#define RDES0_SA_FILTER_FAIL    BIT(13)
#define RDES0_DESCRIPTOR_ERROR  BIT(14)
#define RDES0_ERROR_SUMMARY     BIT(15)
#define RDES0_FRAME_LEN_MASK    (0x3FFF << 16)/*GENMASK(29, 16)*/
#define RDES0_FRAME_LEN_SHIFT   16
#define RDES0_DA_FILTER_FAIL    BIT(30)
#define RDES0_OWN               BIT(31)
            /* RDES1 */
#define RDES1_BUFFER1_SIZE_MASK       (GENMASK(10, 0)&(~0xf))
#define RDES1_BUFFER2_SIZE_MASK       GENMASK(21, 11)
#define RDES1_BUFFER2_SIZE_SHIFT      11
#define RDES1_SECOND_ADDRESS_CHAINED  BIT(24)
#define RDES1_END_RING                BIT(25)
#define RDES1_DISABLE_IC              BIT(31)

#define RX_FRAME_ERROR_TYPE1 \
            (RDES0_DESCRIPTOR_ERROR | RDES0_SA_FILTER_FAIL | \
            RDES0_OVERFLOW_ERROR | RDES0_IPC_CSUM_ERROR | RDES0_COLLISION | \
            RDES0_CRC_ERROR | RDES0_DRIBBLING | RDES0_LENGTH_ERROR | RDES0_MII_ERROR)

#define RX_FRAME_ERROR_TYPE2 \
            (RDES0_LENGTH_ERROR | RDES0_MII_ERROR)

/* Normal transmit descriptor defines */
/* TDES0 */
#define TDES0_DEFERRED              BIT(0)
#define TDES0_UNDERFLOW_ERROR       BIT(1)
#define TDES0_EXCESSIVE_DEFERRAL    BIT(2)
#define TDES0_COLLISION_COUNT_MASK  GENMASK(6, 3)
#define TDES0_VLAN_FRAME            BIT(7)
#define TDES0_EXCESSIVE_COLLISIONS  BIT(8)
#define TDES0_LATE_COLLISION        BIT(9)
#define TDES0_NO_CARRIER            BIT(10)
#define TDES0_LOSS_CARRIER          BIT(11)
#define TDES0_PAYLOAD_ERROR         BIT(12)
#define TDES0_FRAME_FLUSHED         BIT(13)
#define TDES0_JABBER_TIMEOUT        BIT(14)
#define TDES0_ERROR_SUMMARY         BIT(15)
#define TDES0_IP_HEADER_ERROR       BIT(16)
#define TDES0_TIME_STAMP_STATUS     BIT(17)
#define TDES0_OWN                   ((UINT32)BIT(31))   /* silence sparse */
/* TDES1 */
#define TDES1_BUFFER1_SIZE_MASK     GENMASK(10, 0)
#define TDES1_BUFFER2_SIZE_MASK     GENMASK(21, 11)
#define TDES1_BUFFER2_SIZE_SHIFT    11
#define TDES1_TIME_STAMP_ENABLE     BIT(22)
#define TDES1_DISABLE_PADDING       BIT(23)
#define TDES1_SECOND_ADDRESS_CHAINED  BIT(24)
#define TDES1_END_RING              BIT(25)
#define TDES1_CRC_DISABLE           BIT(26)
#define TDES1_CHECKSUM_INSERTION_MASK   GENMASK(28, 27)
#define TDES1_CHECKSUM_INSERTION_SHIFT  27
#define TDES1_FIRST_SEGMENT         BIT(29)
#define TDES1_LAST_SEGMENT          BIT(30)
#define TDES1_INTERRUPT             BIT(31)

#define FFILTER_PROMIS_MODE   (1 << 0) /* Promiscuous Mode */
#define FFILTER_HASH_MCAST    (1 << 2)
#define FFILTER_PASSALL_MCAST (1 << 4)

typedef struct gmac_desc 
{
    volatile UINT32 status;
    volatile UINT32 ctrl;
    volatile UINT32 addr;
    volatile UINT32 next;
}GMAC_DESC;

/*
 * Private adapter context structure.
 */

typedef struct gmac_drv_ctrl
    {
    END_OBJ          gmacEndObj;
    VXB_DEVICE_ID    gmacDev;
    void *           gmacBar;
    void *           gmacHandle;
    FUNCPTR          clkSetup;
    FUNCPTR          ifCached;
    void *           gmacMuxDevCookie;

    JOB_QUEUE_ID     gmacJobQueue;
    QJOB             gmacIntJob;   
    volatile atomicVal_t      gmacIntPending;
    volatile atomicVal_t      gmacRxPending;
    volatile atomicVal_t      gmacTxPending;

    BOOL             gmacPolling;
    M_BLK_ID         gmacPollBuf;
    UINT32           gmacIntrs;
    UINT8            gmacAddr[ETHER_ADDR_LEN];

    END_CAPABILITIES gmacCaps;
    END_IFDRVCONF    gmacEndStatsConf;
    END_IFCOUNTERS   gmacEndStatsCounters;

    UINT32           gmacInErrors;
    UINT32           gmacInDiscards;
    UINT32           gmacInUcasts;
    UINT32           gmacInMcasts;
    UINT32           gmacInBcasts;
    UINT32           gmacInOctets;
    UINT32           gmacOutErrors;
    UINT32           gmacOutUcasts;
    UINT32           gmacOutMcasts;
    UINT32           gmacOutBcasts;
    UINT32           gmacOutOctets;

    /* Begin MII/ifmedia required fields. */

    END_MEDIALIST *  gmacMediaList;
    END_ERR          gmacLastError;
    UINT32           gmacCurMedia;
    UINT32           gmacCurStatus;
    VXB_DEVICE_ID    gmacMiiBus;
    VXB_DEVICE_ID    gmacMiiDev;
    FUNCPTR          gmacMiiPhyRead;
    FUNCPTR          gmacMiiPhyWrite;
    int              gmacMiiPhyAddr;

    /* End MII/ifmedia required fields */

   /* DMA tags and maps. */


    GMAC_DESC *       gmacRxDescMem;
    GMAC_DESC *       gmacTxDescMem;
    M_BLK_ID         gmacTxMblk[GMAC_TX_DESC_CNT];
    M_BLK_ID         gmacRxMblk[GMAC_RX_DESC_CNT];

    UINT32           gmacTxProd;
    UINT32           gmacTxCons;
    UINT32           gmacTxFree;
    UINT32           gmacRxIdx;
    UINT32           gmacTxLast;
    UINT32           gmacTxStall;

    SEM_ID           gmacMiiSem;
    spinlockIsr_t    gmacLock;
    int              gmacMaxMtu;
    BOOL             gmacExPhy; /* if true,  indicates it's using external PHY */

    SEM_ID              gmacDevSem;
    int              unit;

    /* MDC clock access PHY. [1.0MHz ~2.5MHz] */
    
    UINT32           clkMDC;
    void            (*gmacAddrSet)(UINT8* macAddr);
    
    UINT32           gmacDmaStatus;
    } GMAC_DRV_CTRL;

#define GMAC_CACHE_DRV_VIRT_TO_PHYS(adrs)   \
    CACHE_DRV_VIRT_TO_PHYS(&cacheDmaFuncs, (void *) (adrs))

#define GMAC_BAR(p)        ((GMAC_DRV_CTRL *)(p)->pDrvCtrl)->gmacBar
#define GMAC_HANDLE(p)     ((GMAC_DRV_CTRL *)(p)->pDrvCtrl)->gmacHandle

#define CSR_READ_4(pDev, addr)            \
        vxbRead32(GMAC_HANDLE(pDev),       \
                 (UINT32 *)((char *)GMAC_BAR(pDev) + addr))

#define CSR_WRITE_4(pDev, addr, data)     \
        vxbWrite32(GMAC_HANDLE(pDev),      \
                  (UINT32 *)((char *)GMAC_BAR(pDev) + addr), data)

#define CSR_SETBIT_4(pDev, offset, val)   \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) | (val))

#define CSR_CLRBIT_4(pDev, offset, val)   \
        CSR_WRITE_4(pDev, offset, CSR_READ_4(pDev, offset) & ~(val))



#ifdef __cplusplus
}
#endif

#endif /* __INCvxbFtGmacEndh */
