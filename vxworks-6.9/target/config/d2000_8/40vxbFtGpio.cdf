/* 40vxbFtGpio.cdf - GPIO configuration file */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 

Component DRV_FTGPIO {
        NAME            FT GPIO controller driver
        SYNOPSIS        FT GPIO controller driver
        _CHILDREN       FOLDER_DRIVERS
        _INIT_ORDER     hardWareInterFaceBusInit
        INIT_RTN        vxbFtGpioDrvRegister();
        PROTOTYPE       void vxbFtGpioDrvRegister (void);
        REQUIRES        INCLUDE_PLB_BUS  
        INIT_AFTER      INCLUDE_PLB_BUS
}

