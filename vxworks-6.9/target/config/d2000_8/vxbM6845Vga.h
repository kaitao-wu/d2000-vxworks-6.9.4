/* vxbM6845Vga.h - PC Keyboard and VGA Controller header file */

/* 
 * Copyright (c) 2007, 2010 Wind River Systems, Inc.
 * 
 * The right to copy, distribute, modify or otherwise make use
 * of this software may be licensed only pursuant to the terms
 * of an applicable Wind River license agreement.
 */

/*
modification history
--------------------
01c,25jan10,jc0  add videoMemHandle for video memory handler.
01b,17aug07,pmr  new register access methods
01a,20feb07,pmr  adapted for VxBus from version 01d.
*/

#ifndef __INCvxbM6845Vgah
#define __INCvxbM6845Vgah

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _ASMLANGUAGE

#include <tyLib.h>
#include <vxBusLib.h>
#include "pcConsole.h"
#include <hwif/vxbus/vxBus.h>

#define NPARS   16          /* number of escape parameters */

/* method ids */
METHOD_DECL(vgaScreenRev);
METHOD_DECL(vgaCurAttrib);
METHOD_DECL(vgaCursorSet);
METHOD_DECL(vgaCursorMove);
METHOD_DECL(kbdInit);
METHOD_DECL(kbdNumSet);
METHOD_DECL(pcConBeep);

/* vga console device descriptor */

typedef struct 
{
    VXB_DEVICE_ID pDev;
    TY_DEV *    pTyDev;
    UCHAR * memBase;        /* video memory base */
    UCHAR * selReg;         /* select register */
    UCHAR * valReg;         /* value register */
    UCHAR       curSt, curEd;           /* current cursor mode */
    int     row, col;       /* current cursor position */
    UCHAR * curChrPos;          /* current character position */
    UCHAR   curAttrib;      /* current attribute  */
    UCHAR   defAttrib;          /* current default attribute */
    int     nrow, ncol;     /* current screen geometry */
    int     scst, sced;     /* scroll region from to */
    BOOL    rev;            /* revarse mode char */
    BOOL    autoWrap;       /* auto Wrap mode */
    BOOL    sv_rev;         /* saved revarse mode char */
    int     sv_row, sv_col;     /* saved cursor position */
    UCHAR   sv_curAttrib;       /* saved attributes */
    BOOL    scrollCheck;        /* scroll check */
    UCHAR * charSet;            /* character set Text or Graphics */
    int     vgaMode;            /* added to support graphics Mode */
    BOOL    colorMode;          /* color mode MONO / COLOR */
    BOOL    insMode;            /* insert mode on / off */
    char    tab_stop [80];          /* tab stop mark */
    char    escResp[10];        /* esc sequence response buffer */
    UINT16  escFlags;       /* 16 bit escape flags */
    int     escParaCount;       /* seen escape parameters (count) */
    int     escPara[NPARS];     /* parameters */
    BOOL    escQuestion;        /* ? mark in escape sequence */
    void *  regHandle;      /* register access map handle */
    void    *videoMemHandle;    /* memory access map handle */
} M6845_CON_DEV;

/* monitor definitions */

#define TEXT_SET        0   /* Normal text set */
#define GRAPHICS_VT100_SET  1   /* vt100 graphics set */
#define IBM_GRAPHICS_SET    2   /* IBM graphics character set */
#define TEXT_MODE       0   /* monitor in text mode */
#define GRAPHICS_MODE       1   /* monitor in graphics mode */
#define INSERT_MODE_OFF     0   /* character insert mode off */
#define INSERT_MODE_ON      1   /* character insert mode on */
#define FG_ATTR_MASK        0x07    /* foreground attribute mask */
#define BG_ATTR_MASK        0x70    /* background attribute mask */
#define INT_BLINK_MASK      0x88    /* intensity and blinking mask */
#define FORWARD         1   /* scroll direction forward */
#define BACKWARD        0       /* scroll direction backward */

/* escape flags */

#define  ESC_NORMAL             0x0001  /* normal state */
#define  ESC_ESC                0x0002  /* ESC state */
#define  ESC_BRACE              0x0004  /* ESC [ state */
#define  ESC_GET_PARAMS         0x0008  /* ESC [ n state */
#define  ESC_GOT_PARAMS         0x0010  /* ESC [ n;n;n; state */
#define  ESC_FUNC_KEY           0x0020  /* ESC [ [ state */
#define  ESC_HASH               0x0040  /* ESC # state */
#define  ESC_SET_TEXT           0x0080  /* ESC ( state */
#define  ESC_SET_GPRAHICS       0x0100  /* ESC ) state */

#define  M6845_SEL_REG          0
#define  M6845_CTRL_REG         1
#define  M6845_CHR              2

/* video memory size */

#define  M6845_VIDEO_MEM        2048    
#endif  /* _ASMLANGUAGE */

#ifdef __cplusplus
}
#endif

#endif /* __INCvxbM6845Vgah */
