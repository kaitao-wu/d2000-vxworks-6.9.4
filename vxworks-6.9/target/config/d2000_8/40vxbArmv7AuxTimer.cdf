/* 40vxbArmv7AuxTimer.cdf - ARMV7A aux timer configuration file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */



Component   DRV_ARM_GEN_AUX_TIMER {
    NAME        armv7a Aux Timer Driver
    SYNOPSIS    armv7a Aux Timer Driver
    _CHILDREN   FOLDER_DRIVERS
    _INIT_ORDER hardWareInterFaceBusInit
    INIT_RTN    armv7AuxTimerRegister();
    PROTOTYPE   void armv7AuxTimerRegister (void);
    REQUIRES    INCLUDE_PLB_BUS \
                INCLUDE_VXB_AUX_CLK
}

