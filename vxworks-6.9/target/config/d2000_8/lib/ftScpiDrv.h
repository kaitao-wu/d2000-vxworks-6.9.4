/* ftScpiDrv.h - scpi driver */

/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it;
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef __INCftScpiDrvh
#define __INCftScpiDrvh
 
/*****************************************************************************
*
* scpiGetDvfsInfo -  Get the DVFS capabilities of a given power domain
*
* This function get the Operating Points (OPPs) list supported by the power domain,
* and print the OPPs list like this:
* 
* index: 0, freq: 575000000 Hz, volt: 800 mV
* index: 1, freq: 766000000 Hz, volt: 800 mV
* index: 2, freq: 1150000000 Hz, volt: 800 mV
* index: 3, freq: 2300000000 Hz, volt: 800 mV
*
* PARAMETER: 
*           domain: the cluster number
* 
* RETURNS: N/A
*
* ERRNO: N/A
*/
void scpiGetDvfsInfo(UINT8 domain);


/*****************************************************************************
*
* scpiGetDvfsIdx -  Get the current Operating Point for the given Power Domain
*
* This function get an index of the Operating Point List.
*
* PARAMETER: 
*           domain: the cluster number
*
* ERRNO: N/A
*/
int scpiGetDvfsIdx(UINT8 domain);

/*****************************************************************************
*
* scpiSetDvfsIdx -  Set the Operating Point of a given Power Domain
*
* This function set the Operating Point index
*
* PARAMETER: 
*           domain: the cluster number
*           index:  index to the Operating Point List
*
* ERRNO: N/A
*/
void scpiSetDvfsIdx(UINT8 domain, UINT8 index);


/*****************************************************************************
*
* scpiGetFreqVal -  get the frequency value of a given Power Domain
*
* This function get the frequency value of a given Power Domain
*
* PARAMETER: 
*           domain: the cluster number
*
* ERRNO: N/A
*/
UINT32 scpiGetFreqVal(UINT16 domain);

/*****************************************************************************
*
* scpiGetTempVal -  get the temperature sensor value
*
* This function get the temperature sensor value
*
* PARAMETER: 
*
* ERRNO: N/A
*/
long long scpiGetTempVal(void);

#endif /* __INCftScpiDrvh */