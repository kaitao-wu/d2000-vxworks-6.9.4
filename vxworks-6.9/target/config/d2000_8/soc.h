/* soc.h - soc chip file */
                                                                                
/*
 *  
 * This program is OPEN SOURCE software: you can redistribute it and/or modify it; 
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY;  
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */
 
#ifndef __INCsoc_h
#define __INCsoc_h

#ifdef __cplusplus
extern "C"
{
#endif


#define SOC_UART_CLK              (48000000)   /* UART clock       */
#define SOC_I2C_CLK               (48000000)   /* I2C clock       */

#define UART_0_BASE_ADR             (0x28000000)          /* UART 0 base address */
#define UART_1_BASE_ADR             (0x28001000)          /* UART 1 base address */
#define UART_2_BASE_ADR             (0x28002000)          /* UART 2 base address */
#define UART_3_BASE_ADR             (0x28003000)          /* UART 3 base address */

#define GPIO_0_BASE_ADR             (0x28004000)          /* GPIO 0 base address */
#define GPIO_1_BASE_ADR             (0x28005000)          /* GPIO 1 base address */

#define GIC_BASE_ADR                (0x29a00000)

#define INT_VEC_UART0INTR      38
#define INT_VEC_UART1INTR      39
#define INT_VEC_UART2INTR      40
#define INT_VEC_UART3INTR      41
#define INT_VEC_GPIO0INTR      42
#define INT_VEC_GPIO1INTR      43

/*CAN Baudrate*/
enum can_bitrate{
    CAN_BRATE_5    = 5,    /*  */
    CAN_BRATE_10   = 10,   /* */
    CAN_BRATE_20   = 20,   /*  */
    CAN_BRATE_40   = 40,   /*  */
    CAN_BRATE_50   = 50,   /*  */
    CAN_BRATE_80   = 80,   /*  */
    CAN_BRATE_100  = 100,  /*  */
    CAN_BRATE_125  = 125,  /*  */
    CAN_BRATE_200  = 200,  /*  */
    CAN_BRATE_250  = 250,  /*  */
    CAN_BRATE_400  = 400,  /*  */
    CAN_BRATE_500  = 500,  /*  */
    CAN_BRATE_800  = 800,  /*  */
    CAN_BRATE_1000 = 1000, /*  */
};

#define I2C_0_INTR_IRQ                      (44)
#define I2C_1_INTR_IRQ                      (45)
#define I2C_2_INTR_IRQ                      (46)
#define I2C_3_INTR_IRQ                      (47)

#define REG_I2C0_BASE       (0x28006000)
#define REG_I2C1_BASE       (0x28007000)
#define REG_I2C2_BASE       (0x28008000)
#define REG_I2C3_BASE       (0x28009000)


#define FT_SDHC0_BASE (0x28207c00)
#define FTCAN_BRATE_DEFAULT  (CAN_BRATE_100*1000)

#define FTCAN0_BASE  0x28207000
#define FTCAN0_IRQ   119
#define FTCAN0_BITRATE FTCAN_BRATE_DEFAULT


#define FTCAN1_BASE  0x28207400
#define FTCAN1_IRQ   123
#define FTCAN1_BITRATE FTCAN_BRATE_DEFAULT

#define FTCAN2_BASE  0x28207800
#define FTCAN2_IRQ   124
#define FTCAN2_BITRATE FTCAN_BRATE_DEFAULT

#define FTSCPI_BASE  0x2a000000
#define FTSCPI_IRQ   80
#define FTSCPI_RX_MEM_BASE 0x2a007000
#define FTSCPI_TX_MEM_BASE 0x2a007400

/* pad pin multi-function demux component */
#define PIN_DEMUX_BASE 0x28180000
#define REG200 0x200
#define REG204 0x204
#define REG208 0x208
#define CAN_TXD_0 (1<<24)
#define CAN_RXD_0 (1<<12)
#define CAN_TXD_1 (1<<20)
#define CAN_RXD_1 (1<<8)
#define CAN_TXD_2 (1<<16)
#define CAN_RXD_2 (1<<4)
#define I2C_2_SCL (2<<8) /* can_rxd_1 = I2C_2_scl:  ONLY choose one component! */
#define I2C_2_SDA (2<<4) /* can_rxd_2 = i2c_2_sda */

#define FUNC1_GPIO1_PORTA_5 (1<<16)
#define FUNC1_GPIO1_PORTA_6 (1<<12)

/*
* SOC PCI                                 size              usage
* 0x000_40000000~0x000_4FFFFFFF           256MB          Config
* 0x000_50000000~0x000_57FFFFFF           128MB          i/o
* 0x000_58000000~0x000_7FFFFFFF           640MB          mem32
* 0x010_00000000~0x01F_FFFFFFFF           64GB           mem64
*/

#define FT_PCI_CONFIG_ADDR 0x40000000
#define FT_PCI_CONFIG_LEN  0x10000000  /*256MB*/
#define CPU_IO_SPACE_OFFSET 0x50000000

#define FT_SECONDARY_BOOT_ADDR 0x8007fff0

typedef unsigned char   u8,  __u8, unchar;
typedef char        s8,  __s8;
typedef unsigned short  u16, __u16, __be16;
typedef short       s16, __s16;
typedef unsigned int    u32, __u32, __be32, uint;
typedef int     s32, __s32;
typedef unsigned long long u64;

/* ARM ARCH7 later. */
#define ISB __asm__ volatile ("isb sy" : : : "memory")
#define DSB __asm__ volatile ("dsb sy" : : : "memory")
#define DMB __asm__ volatile ("dmb sy" : : : "memory")
#define isb()   ISB
#define dsb()   DSB
#define dmb()   DMB
/*
 * Generic virtual read/write.  Note that we don't support half-word
 * read/writes.  We define __arch_*[bl] here, and leave __arch_*w
 * to the architecture specific code.
 */
#define __arch_getb(a)          (*(volatile unsigned char *)(a))
#define __arch_getw(a)          (*(volatile unsigned short *)(a))
#define __arch_getl(a)          (*(volatile unsigned int *)(a))
#define __arch_getq(a)          (*(volatile unsigned long long *)(a))

#define __arch_putb(v,a)        (*(volatile unsigned char *)(a) = (v))
#define __arch_putw(v,a)        (*(volatile unsigned short *)(a) = (v))
#define __arch_putl(v,a)        (*(volatile unsigned int *)(a) = (v))
#define __arch_putq(v,a)        (*(volatile unsigned long long *)(a) = (v))

#define __raw_writeb(v,a)   __arch_putb(v,a)
#define __raw_writew(v,a)   __arch_putw(v,a)
#define __raw_writel(v,a)   __arch_putl(v,a)
#define __raw_writeq(v,a)   __arch_putq(v,a)

#define __raw_readb(a)      __arch_getb(a)
#define __raw_readw(a)      __arch_getw(a)
#define __raw_readl(a)      __arch_getl(a)
#define __raw_readq(a)      __arch_getq(a)

/*
 * TODO: The kernel offers some more advanced versions of barriers, it might
 * have some advantages to use them instead of the simple one here.
 */
#define mb()        dsb()
#define __iormb()   dmb()
#define __iowmb()   dmb()

#define writeb(v,c) ({ u8  __v = v; __iowmb(); __arch_putb(__v,c); __v; })
#define writew(v,c) ({ u16 __v = v; __iowmb(); __arch_putw(__v,c); __v; })
#define writel(v,c) ({ u32 __v = v; __iowmb(); __arch_putl(__v,c); __v; })
/*#define writeq(v,c)   ({ u64 __v = v; __iowmb(); __arch_putq(__v,c); __v; })*/


#define readb(c)    ({ u8  __v = __arch_getb(c); __iormb(); __v; })
#define readw(c)    ({ u16 __v = __arch_getw(c); __iormb(); __v; })
#define readl(c)    ({ u32 __v = __arch_getl(c); __iormb(); __v; })
/*#define readq(c)  ({ u64 __v = __arch_getq(c); __iormb(); __v; })*/


/* assembly functions */
void sys_icc_igrpen1_set(unsigned int);
unsigned int sys_icc_igrpen1_get(void);
void sys_icc_ctlr_set(unsigned int);
unsigned int sys_icc_hppir1_get(void);
void sys_icc_eoir1_set(unsigned int);
void sys_icc_pmr_set(unsigned int);
unsigned int sys_icc_iar1_get(void);
void sys_icc_dir_set(unsigned int);
void sys_icc_bpr1_set(unsigned int);
unsigned int sys_icc_sre_get(void);
unsigned int vxMpidrGet();
unsigned int sys_dfar_get();
unsigned int sys_dfsr_get();
unsigned int sys_ifar_get();
unsigned int sys_ifsr_get();


#ifdef __cplusplus
}
#endif

#endif  /* __INCsoc_h */

